var RestBatchExecutor = (function () {
    function RestBatchExecutor(appWebUrl, authHeader) {
        this.changeRequests = [];
        this.getRequests = [];
        this.resultsIndex = [];
        this.appWebUrl = appWebUrl;
        this.authHeader = authHeader;
    }
    RestBatchExecutor.prototype.loadChangeRequest = function (request) {
        request.resultToken = this.getUniqueId();
        this.changeRequests.push($.extend({},request));
        return request.resultToken;
    };

    RestBatchExecutor.prototype.loadRequest = function (request) {
        request.resultToken = this.getUniqueId();
        this.getRequests.push($.extend({},request));
        return request.resultToken;
    };

    RestBatchExecutor.prototype.executeAsync = function (info, success, error) {
        var dfd = $.Deferred();
        var payload = this.buildBatch();

        if (info && info.crossdomain === true) {
            this.executeCrossDomainAsync(payload).done(function (result) {
                dfd.resolve(result);
            }).fail(function (err) {
                dfd.reject(err);
            });
        } else {
            this.executeJQueryAsync(payload).done(function (result) {
                dfd.resolve(result);
            }).fail(function (err) {
                dfd.reject(err);
            });
        }

        return dfd;
    };

    RestBatchExecutor.prototype.executeCrossDomainAsync = function (batchBody) {
        var _this = this;
        var dfd = $.Deferred();
        var batchUrl = this.appWebUrl + '/_api/$batch';
        var executor = new SP.RequestExecutor(this.appWebUrl);
        var info = {
            url: batchUrl,
            method: 'POST',
            body: batchBody,
            headers: this.getRequestHeaders(),
            success: function (data) {
                var results = _this.buildResults(data.body);
                _this.clearRequests();
                dfd.resolve(results);
            },
            error: function (err) {
                _this.clearRequests();
                dfd.reject(err);
            }
        };

        executor.executeAsync(info);
        return dfd;
    };

    RestBatchExecutor.prototype.executeJQueryAsync = function (batchBody) {
        var _this = this;
        var dfd = $.Deferred();
        var batchUrl = this.appWebUrl + '/_api/$batch';

        $.ajax({
            'url': batchUrl,
            'type': 'POST',
            'data': batchBody,
            'headers': this.getRequestHeaders(),
            'success': function (data) {
                var results = _this.buildResults(data);
                _this.clearRequests();
                dfd.resolve(results);
            },
            'error': function (err) {
                _this.clearRequests();
                dfd.reject(err);
            }
        });

        return dfd;
    };

    RestBatchExecutor.prototype.getRequestHeaders = function () {
        var header = {};
        header['accept'] = 'application/json;odata=verbos';
        header['content-type'] = 'multipart/mixed; boundary=batch_8890ae8a-f656-475b-a47b-d46e194fa574';
        header[Object.keys(this.authHeader)[0]] = this.authHeader[Object.keys(this.authHeader)[0]];

        return header;
    };

    RestBatchExecutor.prototype.getBatchRequestHeaders = function (headers, batchCommand) {
        var isAccept = false;
        if (headers) {
            $.each(Object.keys(headers), function (k, v) {
                batchCommand.push(v + ': ' + headers[v]);
                if (!isAccept) {
                    isAccept = (v.toUpperCase() === 'ACCEPT');
                }
                ;
            });
        }

        if (!isAccept) {
            batchCommand.push('accept:application/json;odata=verbose');
        }
    };

    RestBatchExecutor.prototype.buildBatch = function () {
        var _this = this;
        var batchCommand = [];
        var batchBody;

        $.each(this.changeRequests, function (k, v) {
            _this.buildBatchChangeRequest(batchCommand, v, k);
            _this.resultsIndex.push(v.resultToken);
        });

        batchCommand.push('--changeset_f9c96a07-641a-4897-90ed-d285d2dbfc2e--');

        $.each(this.getRequests, function (k, v) {
            _this.buildBatchGetRequest(batchCommand, v, k);
            _this.resultsIndex.push(v.resultToken);
        });

        batchBody = batchCommand.join('\r\n');

        //embed all requests into one batch
        batchCommand = new Array();
        batchCommand.push('--batch_8890ae8a-f656-475b-a47b-d46e194fa574');
        batchCommand.push('Content-Type: multipart/mixed; boundary=changeset_f9c96a07-641a-4897-90ed-d285d2dbfc2e');
        batchCommand.push('Content-Length: ' + batchBody.length);
        batchCommand.push('Content-Transfer-Encoding: binary');
        batchCommand.push('');
        batchCommand.push(batchBody);
        batchCommand.push('');
        batchCommand.push('--batch_8890ae8a-f656-475b-a47b-d46e194fa574--');

        batchBody = batchCommand.join('\r\n');
        return batchBody;
    };

    RestBatchExecutor.prototype.buildBatchChangeRequest = function (batchCommand, request, batchIndex) {
        batchCommand.push('--changeset_f9c96a07-641a-4897-90ed-d285d2dbfc2e');
        batchCommand.push('Content-Type: application/http');
        batchCommand.push('Content-Transfer-Encoding: binary');
        batchCommand.push('Content-ID: ' + (batchIndex + 1));
        batchCommand.push(request.binary ? 'processData: false' : 'processData: true');
        batchCommand.push('');
        batchCommand.push(request.verb.toUpperCase() + ' ' + request.endpoint + ' HTTP/1.1');
        this.getBatchRequestHeaders(request.headers, batchCommand);
        if (!request.binary && request.payload) {
            batchCommand.push('Content-Type: application/json;odata=verbose');
        }
        if (request.binary && request.payload) {
            batchCommand.push('Content-Length :' + request.payload.byteLength);
        }
        batchCommand.push('');

        if (request.payload) {
            batchCommand.push(request.binary ? request.payload : JSON.stringify(request.payload));
            batchCommand.push('');
        }
    };

    RestBatchExecutor.prototype.buildBatchGetRequest = function (batchCommand, request, batchIndex) {
        batchCommand.push('--batch_8890ae8a-f656-475b-a47b-d46e194fa574');
        batchCommand.push('Content-Type: application/http');
        batchCommand.push('Content-Transfer-Encoding: binary');
        batchCommand.push('Content-ID: ' + (batchIndex + 1));
        batchCommand.push('');
        batchCommand.push('GET ' + request.endpoint + ' HTTP/1.1');
        this.getBatchRequestHeaders(request.headers, batchCommand);
        batchCommand.push('');
    };

    RestBatchExecutor.prototype.buildResults = function (responseBody) {
        var _this = this;
        var responseBoundary = responseBody.substring(0, 52);
        var resultTemp = responseBody.split(responseBoundary);
        var resultData = [];

        $.each(resultTemp, function (k, v) {
            if (v.indexOf('\r\nContent-Type: application/http\r\nContent-Transfer-Encoding: binary') == 0) {
                var responseTemp = v.split('\r\n');
                var batchResult = new RestBatchResult();

                //grab just the http status code
                batchResult.status = responseTemp[4].substr(9, 3);

                //based on the status pull the result from response
                batchResult.result = _this.getResult(batchResult.status, responseTemp);

                //assign return token to result
                resultData.push({ id: _this.resultsIndex[k - 1], result: batchResult });
            }
        });

        return resultData;
    };

    RestBatchExecutor.prototype.getResult = function (status, response) {
        switch (status) {
            case '400':
            case '404':
            case '500':
            case '200':
                return this.parseJSON(response[7]);
            case '204':
            case '201':
                return this.parseJSON(response[9]);
            default:
                return this.parseJSON(response[4]);
        }
    };

    RestBatchExecutor.prototype.getUniqueId = function () {
        return (this.randomNum() + this.randomNum() + this.randomNum() + this.randomNum() + this.randomNum() + this.randomNum() + this.randomNum() + this.randomNum());
    };

    RestBatchExecutor.prototype.randomNum = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };

    RestBatchExecutor.prototype.clearRequests = function () {
        while (this.changeRequests.length) {
            this.changeRequests.pop();
        }
        ;
        while (this.getRequests.length) {
            this.getRequests.pop();
        }
        ;
        while (this.resultsIndex.length) {
            this.resultsIndex.pop();
        }
        ;
    };

    RestBatchExecutor.prototype.parseJSON = function (jsonString) {
        try  {
            var o = JSON.parse(jsonString);

            // Handle non-exception-throwing cases:
            // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
            // but... JSON.parse(null) returns 'null', and typeof null === "object",
            // so we must check for that, too.
            if (o && typeof o === 'object' && o !== null) {
                return o;
            }
        } catch (e) {
        }

        return jsonString;
    };
    return RestBatchExecutor;
})();

var RestBatchResult = (function () {
    function RestBatchResult() {
        this.status = '';
        this.result = null;
    }
    return RestBatchResult;
})();
var BatchRequest = (function () {
    function BatchRequest() {
        this.resultToken = '';
        this.endpoint = '';
        this.payload = '';
        this.binary = false;
        this.headers = null;
        this.verb = 'GET';
    }
    return BatchRequest;
})();

var users = [],ims=[],locations=[],focals=[];
var approver=false;
var current_inv = null;
var cache_active = true;
var investments = [], allinvestments=[],decisions=[];
var leave_days = [];
var approved_leave_days = [];
var siteUrl=_spPageContextInfo.webAbsoluteUrl+'/_api/web/';
var psitelst=siteUrl+'lists/GetByTitle';
var load='init';
var leaveurl=psitelst+'(\'Leave Requests\')/items?$select=*,Author/Title,Supervisor/Title&$expand=Supervisor,Author&$filter=SupervisorId eq '+_spPageContextInfo.userId;
var memourl =psitelst+'(\'Business Case Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_';
var travelurl =psitelst + '(\'TR\')/items?$select=*,Focal_x0020_Person/Title,Author/Title,Focal_x0020_PersonId&$expand=Focal_x0020_Person,Author';
var tbtravel=null,tbdecmemo=null,travels=[],leaves=[];
var travform={id:null,purpose:'',dest1:'',dest2:'',date1:'',date2:'',destinations:'',hostdetails:'',flights:'',accomodation:'',transport:'',luggage:'',otherinfo:'',focalid:null,focal:'',status:''};
var districts=[{index:17,name:'Abudwak',state:'GS',state_id:2},{index:16,name:'Adado',state:'GS',state_id:2},{index:3,name:'Afmadow',state:'JS',state_id:3},{index:10,name:'Aluula',state:'PL',state_id:1},{index:25,name:'Baidoa',state:'SW',state_id:4},{index:27,name:'Baki',state:'SL',state_id:6},{index:41,name:'Balacad',state:'HM',state_id:5},{index:33,name:'Baraawe',state:'HM',state_id:5},{index:2,name:'Bardera',state:'JS',state_id:3},{index:39,name:'Bardheere',state:'JS',state_id:3},{index:6,name:'Beled Haawo',state:'JS',state_id:3},{index:40,name:'Bevarweyne',state:'HM',state_id:5},{index:8,name:'Bosaso',state:'PL',state_id:1},{index:4,name:'Buaale',state:'JS',state_id:3},{index:29,name:'Burao',state:'SL',state_id:6},{index:7,name:'Buuhoodle',state:'PL',state_id:1},{index:30,name:'Buuhoodle',state:'SL',state_id:6},{index:43,name:'Ceel Barde',state:'SW',state_id:4},{index:20,name:'Ceeldheer',state:'GS',state_id:2},{index:11,name:'Dhahar',state:'PL',state_id:1},{index:18,name:'Dhusamareb',state:'GS',state_id:2},{index:45,name:'Dinsoor',state:'SW',state_id:4},{index:19,name:'El Buur',state:'GS',state_id:2},{index:15,name:'Galkayo',state:'GS',state_id:2},{index:37,name:'Garbaharey',state:'JS',state_id:3},{index:12,name:'Garowe',state:'PL',state_id:1},{index:24,name:'Godinlabe',state:'GS',state_id:2},{index:23,name:'Guriel',state:'GS',state_id:2},{index:22,name:'Harardhere',state:'GS',state_id:2},{index:28,name:'Hargeisa',state:'SL',state_id:6},{index:21,name:'Hobyo',state:'GS',state_id:2},{index:26,name:'Jowhar',state:'HM',state_id:5},{index:1,name:'Kismayo',state:'JS',state_id:3},{index:14,name:'Las Anod',state:'SS',state_id:7},{index:13,name:'Las Qorey',state:'SS',state_id:7},{index:5,name:'Luuq',state:'JS',state_id:3},{index:31,name:'Mogadishu',state:'BR',state_id:8},{index:35,name:'Nationwide',state:'NW',state_id:9},{index:36,name:'Non-Specific',state:'NS',state_id:11},{index:9,name:'Qardho',state:'PL',state_id:1},{index:38,name:'Raskamboni',state:'JS',state_id:3},{index:32,name:'Warsheikh',state:'HM',state_id:5},{index:44,name:'Xudul',state:'SW',state_id:4}];
var trform ={id:null,title:'',refcode:'',districts:'',attachments:[],state:'',im1:'',im2:'',im1_id:null,im2_id:null,approval_status:'',proposal:'',objective:'',output:'',est_fund:'',inv_type:'',proc_cycle:'',proc_type:'',prob_stat:'',just:'',rec:'',disabled:true,pillar:'',comments:''};
var decmemo={id:null,title:'',refcode:'',district:'',state:'',im1:'',im2:'',im1_id:null,im2_id:null,status:'',status2:'',output2:'',output3:'',output:'',est_fund:'',inv_type:'',startdate:'',enddate:'',prob_stat:'',email:'',rec:'',phone:'',address:'',background:'',keyact:'',valuemoney:'',sustain:'',conflictsens:'',disabled:true,comments:''};

function loadbatch(){
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(49)/users';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata'};
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getIMs' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(8)/users';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getUsers' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Focal Person\')/items?$select=User/Id,User/Title&$expand=User';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getFocals' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'TR Locations\')/items?$select=Title';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getLocations' });
    batchRequest = new BatchRequest();    
    batchRequest.endpoint = psitelst+'(\'Business Case Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Approval_x0020_Status eq \'Approved\'';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Leave Requests\')/items?$filter=(Status eq \'Approved\') and (AuthorId eq '+_spPageContextInfo.userId+')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getApprovedLeave' });
    
    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });            
            if (command[0].title == 'getIMs') {
                getIMs(v.result.result.value);
            }else if (command[0].title == 'getUsers') {
                getUsers(v.result.result.value);
            }else if (command[0].title == 'getFocals') {
                getFocals(v.result.result.value);
            } else if (command[0].title == 'getLocations') {
                getLocations(v.result.result.value);
            }else if (command[0].title == 'getAprInvestments') {
                getAprInvestments(v.result.result.value);
            }else if (command[0].title == 'getApprovedLeave') {
                getApprovedLeave(v.result.result.value);
            }          
        });     
    }).fail(function (err) {
        onError(err);
    });
}

function getUsers(d){
    $.each(d,function(i,j){
        users.push({name: j.Title,id:j.Id});
    });
}

function getFocals(d){
    $.each(d,function(i,j){
        focals.push({name: j.User.Title,id:j.User.Id});
    });
}

function getIMs(d){
    ims=[];
    $.each(d, function (i, j){
        if(j.Id==_spPageContextInfo.userId) approver=true;
        ims.push({id:j.Id,name:j.Title});
    });
    if(approver) $('.menu-tasks').removeClass('hidden');
    else $('.menu-tasks').addClass('hidden');
}

function getLocations(d){
    locations=[];
    $.each(d, function (i, j){
        locations.push(j.Title);
    });
}


function getAprInvestments(d){
    checkDigestTimeout();
    investments=[];
    $.each(d,function(i,j){   
            investments.push({
                refcode: j.Title,
                title: j.Title0,
                id: j.Id,
                im1: getIM(j.IM_x0028_1_x0029_Id),
                im2: getIM(j.IM_x0028_2_x0029_Id),
                im1_id: parseInt(j.IM_x0028_1_x0029_Id),
                im2_id: parseInt(j.IM_x0028_2_x0029_Id),
                pillar: j.Pillar,
                state: j.Federal_x0020_State,
                district: j.Districts,
                proposal: j.Source_x0020_of_x0020_Proposal,
                output2: j.Output2,
                output3: j.Output3,
                output: j.Output,
                est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
                inv_type: j.Investee_x0020_Type,
                proc_type: j.Proc_x002e__x0020_Type,
                proc_cycle: j.Proc_x002e__x0020_Cycle,
                prob_stat: j.Problem_x0020_Statement,
                just: j.Justification,
                rec: j.Recommendation,
                approval_status:j.Approval_x0020_Status,
                attachments:getAttachmentLinks(j.AttachmentFiles),
                comments:j.Comments
            });

    });
    loadHome();
}


function getApprovedLeave(d){
    $.each(d,function(i,j){
        var app_days =j.Dates.split(', ');
        var app_day = null;
        $(app_days).each(function (idx,val) {
            app_day = moment(val,'DD-MM-YYYY').format('YYYY-MM-DD');
            approved_leave_days.push({
                id: app_day,
                title: 'Leave',
                allDay: true,
                start: app_day,
                className: 'hidden'
            });
        });
    });
}

$(document).ready(function() {
    $('br').remove();
    $('.menu-home').click(function(){
        loadHome();
    });
    $('.menu-forms').click(function(){
        loadForms();
    });
    $('.menu-tasks').click(function(){
        loadTasks();
    });

    $('nav ul li').click(function () {
        changeActiveMenu($(this));
    });
    loadbatch();  

    setTimeout(function(){
        checkDigestTimeout();
    }, 240000);
});

function getCache(key){
    if(localStorage.getItem(key) !== null) {
        return isJson(localStorage.getItem(key));
    }
    else{
        return null;
    }
}

function isJson(item) {
    item = typeof item !== 'string' ? JSON.stringify(item) : item;

    try {
        var json =  JSON.parse(item); if (typeof json === 'object' && json !== null) {
            return json;
        }
        return item;
    } catch (e) {
        return item;
    }
}

function isInt(value) {
    if (isNaN(value)) {
        return false;
    }
    var x = parseFloat(value);
    return (x | 0) === x;
}

function RestCalls(u,f) {
   return $.ajax({
        url: siteUrl+u,
        method: 'GET',
        headers: {'Accept': 'application/json; odata=verbose'},
        success: function (data) {f(data.d); },
        error: onError        
    });
}

function onError(e) {
    swal('Error',e.responseText,'error');
}

function postJson(Uri, payload, success) {
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: { 'Accept': 'application/json;odata=verbose', 'X-RequestDigest': $('#__REQUESTDIGEST').val() },
        success: success,
        error: onError
    });
}

function updateJson(Uri, payload, success) {
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: {
            'Accept': 'application/json;odata=verbose','X-RequestDigest': $('#__REQUESTDIGEST').val(),
            'X-HTTP-Method': 'MERGE','If-Match': '*'
        },
        success: success,
        error: onError
    });
}

function checkDigestTimeout(){
    if(getCache('digest_time')){
        var d_time = moment(getCache('digest_time'));
        if(d_time.add((parseInt(getCache('digest_timeout')) - 300), 'seconds').isSameOrAfter(moment.now())){
            renewDigest();
        }
    }else{
       renewDigest(); 
    }
}

function renewDigest() {
    var item={};
    postJson(_spPageContextInfo.webAbsoluteUrl + '/_api/contextinfo', item, success);
    function success(d){
        $('#__REQUESTDIGEST').val(d.d.GetContextWebInformation.FormDigestValue);
        cache('digest_time', moment().format('DD-MM-YYYY HH:MM:SS'));
        cache('digest_timeout', d.d.GetContextWebInformation.FormDigestTimeoutSeconds);
    }
}

function cache(key,value){
    if(typeof value === 'object' ){
        localStorage.setItem(key, JSON.stringify(value));
    }
    else{
        localStorage.setItem(key, value);
    }
}

var addCommas = function(input){
    // If the regex doesn't match, `replace` returns the string unmodified
    return (input.toString()).replace(
        // Each parentheses group (or 'capture') in this regex becomes an argument
        // to the function; in this case, every argument after 'match'
        /^([-+]?)(0?)(\d+)(.?)(\d+)$/g, function(match, sign, zeros, before, decimal, after) {

            // Less obtrusive than adding 'reverse' method on all strings
            var reverseString = function(string) { return string.split('').reverse().join(''); };

            // Insert commas every three characters from the right
            var insertCommas  = function(string) {

                // Reverse, because it's easier to do things from the left
                var reversed = reverseString(string);

                // Add commas every three characters
                var reversedWithCommas = reversed.match(/.{1,3}/g).join(',');

                // Reverse again (back to normal)
                return reverseString(reversedWithCommas);
            };

            // If there was no decimal, the last capture grabs the final digit, so
            // we have to put it back together with the 'before' substring
            return sign + (decimal ? insertCommas(before) + decimal + after : insertCommas(before + after));
        }
        );
};

var removeCommas = function(input){
    return input.replace(/,/g , '');
};

function goto(url) {
    window.location.href = url;
}
var Tawk_API = Tawk_API || {},
Tawk_LoadStart = new Date();
(function() {
    var s1 = document.createElement('script'),
    s0 = document.getElementsByTagName('script')[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/59de1db4c28eca75e4625715/default';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
})();
Tawk_API.visitor = {
    name: _spPageContextInfo.userDisplayName,
    email: _spPageContextInfo.userEmail,
};

function changeActiveMenu(el){
    $('nav ul li').removeClass('active');
    $(el).addClass('active');
}

function fixIframe(){
    $('.MainIframe').contents().find('body').addClass('ms-fullscreenmode');
    $('.MainIframe').contents().find('#s4-ribbonrow,.od-SuiteNav,.Files-leftNav,.od-TopBar-header.od-Files-header,.pageHeader,.ql-editor,#titlerow .ms-table,.sitePage-uservoice-button,.footer').hide().css('display', 'none');
    $('.MainIframe').contents().find('#titlerow').css('background', 'none !important');
    $('.MainIframe').contents().find('.Files-mainColumn').css('left', '0');
    $('.MainIframe').contents().find('.Files-belowSuiteNav').css('top', '0px');
}


function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function loadTabs(){
    $('ul.tabs li').click(function() {
        fixIframe();
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
    });
}

function getAttachmentLinks(Attachments){  
    var links ='';
    $.each(Attachments, function(index,value){ 
        if(value.ServerRelativeUrl!=null)
            links+='<a target=\'_blank\' href=\''+value.ServerRelativeUrl+'\'><span class=\'fa fa-paperclip\'></span> '+value.FileName+'</a>, ';
 });
    return links;
}

function getIM(id){
    var im =  $.grep(ims, function (element, index) {
        return element.id === id;
    });
    if(im.length === 1){
        return im[0].name;
    }
    else{
        return '';
    }
}

function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('<link rel="stylesheet" href="https://somaliastabilityke.sharepoint.com/sites/ssf2/SiteAssets/SSF/css/app.css" type="text/css" />');
    mywindow.document.write('</head><body >');     
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}
function processHome(){
    loadTabs();
    getMemoCards();
}

function loadHome(){
    $('.loaderwrapper').fadeIn();
    if(getCache('home_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/home.html',function(response, status, xhr) {
            cache('home_html', response);
            processHome();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('home_html'));
        processHome();
    }
}

function loadForms(){
    $('.loaderwrapper').fadeIn();
    if(getCache('forms_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/forms.html',function(response, status, xhr) {
            cache('forms_html', response);
            processForms();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('forms_html'));
        processForms();
    }
}

function processForms(){
    loadTabs();
    $('.loaderwrapper').fadeOut();
    $(users).each(function(index,value){
        if(value.id !== _spPageContextInfo.userId){
            $('#supervisor').append('<option value="'+value.id+'">'+value.name+'</option>');
        }
    });

    $(focals).each(function(index,value){
        $('#focal').append('<option value="'+value.id+'">'+value.name+'</option>');
    });

    $(locations).each(function(i,v){
        $('select.dest').append('<option>'+v+'</option>');
    });

    var approved_leave_days_current_month = $.grep(approved_leave_days, function( a_day ) {
        return moment(a_day.start, 'YYYY-MM-DD').format('M') === moment().format('M');
    });

    $('.days-leave-taken-current-month').text(pad(approved_leave_days_current_month.length+'',2));
    $('.days-leave-taken').text(pad(approved_leave_days.length+'',2));

    $('.btn-submit-leave').click(function () {
        $('#leave-form').data('formValidation').validate();
        if($('#leave-form').data('formValidation').isValid()){
            if(leave_days.length > 0){
                saveLeave();
            }
            else{
                swal('Error!', 'You must select atleast one day on the calendar', 'error');
            }
        }
    });

    $('.btn-submit-travel').click(function () {
        $('#travel-form').data('formValidation').validate();
        var fv = $('#travel-form').data('formValidation');
        if($('#dest3').val()){
            fv.enableFieldValidators('date3', true).revalidateField('date3');
        }
        else{
            fv.enableFieldValidators('date3', false).revalidateField('date3');
        }
        if($('#dest4').val()){
            fv.enableFieldValidators('date4', true).revalidateField('date4');
        }
        else{
            fv.enableFieldValidators('date4', false).revalidateField('date4');
        }

        if($('#travel-form').data('formValidation').isValid()){
            saveTravel();
        }
    });

    validateLeaveForm();
    validateTravelForm();

    $('select').chosen({
        allow_single_deselect: true
    });
    

    $('#datetimepicker').datetimepicker({                         
        format: 'DD/MM/YYYY',                           
        minDate: moment().startOf('day').add(5, 'days')
    }).on('dp.change', function(e) {                            
        $('#datetimepicker1').data('DateTimePicker').minDate(e.date);
        $('#datetimepicker2').data('DateTimePicker').minDate(e.date);
        $('#datetimepicker3').data('DateTimePicker').minDate(e.date);
        $('#travel-form').formValidation('revalidateField', 'date1')
        .formValidation('revalidateField', 'date2')
        .formValidation('revalidateField', 'date3')
        .formValidation('revalidateField', 'date4');
    });
    $('#datetimepicker1,#datetimepicker2,#datetimepicker3').datetimepicker({
        format: 'DD/MM/YYYY',                           
        useCurrent: false,
        minDate: moment().startOf('day').add(5, 'days')
    }).on('dp.change dp.show', function(e) { 
        $('#travel-form').formValidation('revalidateField', 'date1')
        .formValidation('revalidateField', 'date2')
        .formValidation('revalidateField', 'date3')
        .formValidation('revalidateField', 'date4');
    });
    loadCalendar();
}


function loadTasks(){
    $('.loaderwrapper').fadeIn();
    if(getCache('tasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/tasks.html',function(response, status, xhr) {
            cache('tasks_html', response);
            processTasks();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('tasks_html'));
        processTasks();
    }
}

function processTasks(){    
    loadTabs();
    loadbatchTaskcount(); 
}


function getReference() {
    var c_pillar_val = $('#pillar').val();
    var c_state_val = $('#state').val();
    if (c_state_val && c_pillar_val) {
        RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=Id&$filter=Pillar eq \''+encodeURIComponent(c_pillar_val)+'\'&Federal_x0020_State eq \''+encodeURIComponent(c_state_val)+'\'',function(d){
            var id = pad((d.results.length + 1) + '', 2);
            var ref = $('#pillar option:selected').attr('data-initial') + '-' + $('#state option:selected').attr('data-initial') + '-' + id;
            $('#refno').text(ref);
            $('#refno-h').val(ref);
        }); 
    }
}


function getMemoCards(){    
    if(load=='init'){
        load='loaded';
        $('.loaderwrapper').fadeOut();
        loadInvestments();
    }else{
     RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Approval_x0020_Status eq \'Approved\'',
        function(d){
            investments = [];
            $.each(d.results,function(i,j){            
                investments.push({
                    refcode: j.Title,
                    title: j.Title0,
                    id: j.Id,
                    im1: getIM(j.IM_x0028_1_x0029_Id),
                    im2: getIM(j.IM_x0028_2_x0029_Id),
                    im1_id: parseInt(j.IM_x0028_1_x0029_Id),
                    im2_id: parseInt(j.IM_x0028_2_x0029_Id),
                    pillar: j.Pillar,
                    state: j.Federal_x0020_State,
                    district: j.Districts,
                    proposal: j.Source_x0020_of_x0020_Proposal,
                    output2: j.Output2,
                    output3: j.Output3,
                    output: j.Output,
                    est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
                    inv_type: j.Investee_x0020_Type,
                    proc_type: j.Proc_x002e__x0020_Type,
                    proc_cycle: j.Proc_x002e__x0020_Cycle,
                    prob_stat: j.Problem_x0020_Statement,
                    just: j.Justification,
                    rec: j.Recommendation,
                    approval_status:j.Approval_x0020_Status,
                    attachments:getAttachmentLinks(j.AttachmentFiles),
                    comments:j.Comments
                });        
            }); 
            loadInvestments();           
        }); 
    }

}


function loadInvestments(){
    var inv_content = '';
    for(i=0;i<investments.length;i++){
        var inv = investments[i];
        if(i%3 === 0){
            inv_content += '<div class="row">';
        }
        inv_content += '<div class="col-md-4">';
        inv_content += '<div class="box-height" onclick="loadInvestment(\''+inv.refcode+'\')">';
        inv_content += '<div class="col-md-4 h-full div-counter">';
        inv_content += '<p class="p-count">'+pad((i+1)+'',2)+'</p>';
        inv_content += '</div>';
        inv_content += '<div class="col-md-8 h-full div-inv">';
        inv_content += '<p class="p-ref mb5">'+inv.refcode+'</p>';
        inv_content += '<p class="p-title mb5">'+inv.title+'</p>';
        inv_content += '<p class="p-im mb5"><b>IM(1):</b>   '+inv.im1+'</p><p class="m0 p-im"><b>IM(2):</b>   '+inv.im2+'</p></p>';
        inv_content += '</div>';
        inv_content += '</div>';
        inv_content += '</div>';
        if((i - 2) % 3 === 0){
            inv_content += '</div>';
        }
        else if(investments.length - 1 === i){
            inv_content += '</div>';
        }
    }
    $('.loaderwrapper').fadeOut();
    $('#inv-contain').empty().append(inv_content);
}


function getMemoForm(){
    $('select').chosen();
    $('#primaryIM').empty().append('<option value="'+_spPageContextInfo.userId+'">'+_spPageContextInfo.userDisplayName+'</option>');

    $('#secondaryIM').empty();
    $(ims).each(function(index,value){
        $('#secondaryIM').append('<option value="'+value.id+'">'+value.name+'</option>');
    });

    $('#primaryIM,#secondaryIM').trigger('chosen:updated');

    $('select#state').change(function() {
        var state = $(this);
        var selected_districts = $.grep(districts, function(element, index) {
            if ($(state).find('option:selected').attr('data-initial') === 'MR') {
                return true;
            } else {
                return element.state === $(state).find('option:selected').attr('data-initial');
            }
        });
        $('select#district').empty();
        var content = '';
        for (var i = 0; i < selected_districts.length; i++) {
            content += '<option value="' + selected_districts[i].name + '">' + selected_districts[i].name + '</option>';
        }
        $('#district').append(content).trigger('chosen:updated');
    });

    $('#nextbcmemo').click(function() {
        $('#bcmemo').hide();
        $('#bcmemonext').removeClass('hidden').fadeIn();
    });
    $('#bcprevmemo').click(function() {
        $('#bcmemonext').hide();
        $('#bcmemo').removeClass('hidden').fadeIn();
    });
    $('.save-memo-draft').click(function() {
        saveMemoDraft();
    });
    $('.save-memo').click(function() {
        saveMemo();
    });

    $('#state').change(function() {
        getReference();
    });
    $('#pillar').change(function() {
        getReference();
    });

    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    $('#fundceiling').bind('textchange', function(event, previousText) {
        var value = removeCommas($(this).val());
        if ($.isNumeric(value)) {
            $(this).val(addCommas(value));
        } else {
            $(this).val('');
        }
    });
    $('input.upload').on('change', function() {
        var path = $(this).val();
        var filename = path.substr(path.lastIndexOf('\\') + 1);
        $(this).closest('.input-group').find('.inputFiles').val(filename);
    });

    $('#memo-form').modal('show');
}


function saveMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to submit business case memo for review and approval?',
        type: 'warning',
        showCancelButton: true,
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Final');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitMemo();
    });
}

function saveMemoDraft() {
    swal({
        title: 'Are you sure?',
        text: '<p>Only the primary and secondary im can view</p>',
        html: true,
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'Save as Draft',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Draft');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitMemo();
    });
}

function submitMemo(){
    var item = {
        '__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
        'Title': $('#refno-h').val(),
        'Pillar': $('#pillar').val(),
        'Title0':$('#bCasetitle').val(),
        'Federal_x0020_State': $('#state').val(),
        'Districts':$('#district').val().join(', '),            
        'Source_x0020_of_x0020_Proposal':$('#proposalSrc').val(),
        'Output2': $('#output2').val(),
        'Output3': $('#output3').val(),
        'Output': $('#output').val(),
        'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
        'Investee_x0020_Type': $('#invtype').val(),
        'Proc_x002e__x0020_Cycle': $('#procurement_cycle').val(),
        'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
        'Problem_x0020_Statement':$('#probstat').val(),
        'Justification':$('#justification').val(),
        'Recommendation':$('#recomhead').val(),
        'IM_x0028_1_x0029_Id': parseInt($('#primaryIM').val()),
        'IM_x0028_2_x0029_Id': parseInt($('#secondaryIM').val()),
        'Status': $('#submit-type').val(),
    };

    postJson(psitelst + '(\'Business Case Memo\')/items', item, success);
    function success(d){
        $('#memo-form').modal('hide');
        swal('Success','Business Memo created successfully','success');
        checkDigestTimeout();
    }

}

function loadInvestment(refcode){
    $('.loaderwrapper').fadeIn();
    if(getCache('inv_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/investment.html',function(response, status, xhr) {
            cache('inv_html', response);
            processInvestment(refcode);
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('inv_html'));
        processInvestment(refcode);
    }
}




function saveTravel() {
    swal({
        title: 'Travel Request',
        text: 'You want to submit the travel request?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitTravel();
    });
}

function submitTravel(){

    var item = {
        '__metadata': { 'type': 'SP.Data.TRListItem' },
        'Title': 'Travel Request',
        'Purpose': $('#purpose').val(),
        'Dest_x0020_1': $('#dest1').val(),
        'Date_x0020_1': moment($('#date1').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'Dest_x0020_2': $('#dest2').val(),
        'Date_x0020_2': moment($('#date2').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'Destinations': $('#dests').val(),
        'Details_x0020_of_x0020_host': $('#host-details').val(),
        'Flights': $('#flights').val(),
        'Accommodation': $('#accommodation').val(),
        'Internal_x0020_Transport': $('#transport').val(),
        'Exta_x0020_Luggage': $('#luggage').val(),
        'Other_x0020_info': $('#other-info').val(),
        'Focal_x0020_PersonId': $('#focal').val()
    };

    if($('#dest3').val()){
        item.Dest_x0020_3 =  $('#dest3').val();
        item.Date_x0020_3 =  moment($('#date3').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString();
    }
    if($('#dest4').val()){
        item.Dest_x0020_4 =  $('#dest4').val();
        item.Date_x0020_4 =  moment($('#date4').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString();
    }

    postJson(psitelst + '(\'TR\')/items', item, success);
    function success(d){
        $('select').val('').trigger('chosen:updated');
        $('textarea').val('');
        $('input').val('');
        $('#travel-form').data('formValidation').resetForm();
        swal('Success','Travel Request submitted successfully. Awaiting approval','success');
        checkDigestTimeout();
    }
}


function validateLeaveForm() {
    $('#leave-form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            description: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },
            leaveType: {
                validators: {
                    callback: {
                        message: 'The leave type field is required',
                        callback: function(value, validator, $field) {
                            return $('#leave-type').val() !== '';
                        }
                    }
                }
            },
            supervisor: {
                validators: {
                    callback: {
                        message: 'The supervisor field is required',
                        callback: function(value, validator, $field) {
                            return $('#supervisor').val() !== '';
                        }
                    }
                }
            }
        }
    });
}


function validateTravelForm() {
    $('#travel-form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            purpose: {
                validators: {
                    notEmpty: {
                        message: 'The purpose is required'
                    }
                }
            },
            dest1: {
                validators: {
                    callback: {
                        message: 'The destination 1 field is required',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            date1: {
                validators: {
                    notEmpty: {
                        message: 'The date 1 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 1 field is not a valid date'
                    }
                }
            },
            dest2: {
                validators: {
                    callback: {
                        message: 'The destination 2 field is required',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            date2: {
                validators: {
                    notEmpty: {
                        message: 'The date 2 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 2 field is not a valid date'
                    }
                }
            },
            date3: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The date 3 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 3 field is not a valid date'
                    }
                }
            },
            date4: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The date 4 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 4 field is not a valid date'
                    }
                }
            },
            dests: {
                validators: {
                    notEmpty: {
                        message: 'The destinations are required'
                    }
                }
            },
            hostDetails: {
                validators: {
                    notEmpty: {
                        message: 'The host details are required'
                    }
                }
            },
            flights: {
                validators: {
                    notEmpty: {
                        message: 'The flights required field is required'
                    }
                }
            },
            accommodation: {
                validators: {
                    notEmpty: {
                        message: 'The accommodation required field is required'
                    }
                }
            },
            transport: {
                validators: {
                    notEmpty: {
                        message: 'The internal transport required field is required'
                    }
                }
            },
            luggage: {
                validators: {
                    notEmpty: {
                        message: 'The extra luggage to be carried field is required'
                    }
                }
            },
            focal: {
                validators: {
                    callback: {
                        message: 'The focal person is required',
                        callback: function(value, validator, $field) {
                            return $('#focal').val() !== '';
                        }
                    }
                }
            }
        }
    });
}


function loadCalendar(){
    $('#calendar').fullCalendar({
        header: {
            left: '',
            center: 'title',
            right: 'prev,next'
        },
        events: approved_leave_days,
        height: 500,
        fixedWeekCount:false,
        dayClick: function(date, jsEvent, view) {
            var check = moment(date);
            var today = moment.now();
            if(check > today && !isLeaveDay(date.format()))
            {
                if(leave_days.indexOf(date.format()) === -1){
                    leave_days.push(date.format());
                    $('#calendar').fullCalendar( 'addEventSource',[
                    {
                        'id': date.format(),
                        'title': 'Leave',
                        'allDay': true,
                        'start': date.format(),
                        'className': 'hidden'
                    }
                    ]
                    );
                }
                else{
                    leave_days.splice(leave_days.indexOf(date.format()), 1);
                    $(this).removeClass('leave-day-selected');
                    var index = $(this).index();
                    $(this).closest('.fc-bg').next().find('table thead td:eq('+index+')').css('color','#000');
                    $('#calendar').fullCalendar( 'removeEvents', date.format());
                }
                $('.days-leave').text(pad(leave_days.length+'',2));
            }
        },
        eventAfterRender: function(event, element) {
            var index = element.parent().index();
            element.parent().closest('.fc-content-skevaron').prev().find('table tbody tr td:eq('+index+')').addClass('leave-day-selected');
            element.parent().closest('tbody').prev().find('tr td:eq('+index+')').css('color','#fff');
        }
    });
}


function saveLeave() {
    swal({
        title: 'Leave Request',
        text: 'You want to submit the leave request?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitLeave();
    });
}

function submitLeave(){
    leave_days.sort(function(a, b){
        return moment(a,'YYYY-MM-DD') - moment(b,'YYYY-MM-DD');
    });

    leave_days = $.map(leave_days, function(val,i){
        return [moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY')];
    });

    var item = {
        '__metadata': { 'type': 'SP.Data.Leave_x0020_RequestsListItem' },
        'SupervisorId': parseInt($('#supervisor').val()),
        'Days': leave_days.length,
        'Dates': leave_days.join(', '),
        'Description': $('#description').val()
    };
    postJson(_spPageContextInfo.webAbsoluteUrl + '/_api/web/lists/getbytitle(\'Leave Requests\')/items', item, success);
    function success(d){
        $('select').val('').trigger('chosen:updated');
        $('textarea').val('');
        $('#leave-form').data('formValidation').resetForm();
        reloadCalendar();
        swal('Success','Leave Request submitted successfully. Awaiting approval','success');
        checkDigestTimeout();
    }
}


function reloadCalendar(){
    $('.days-leave').text('00');
    leave_days = [];
    $('#calendar').fullCalendar('destroy');
    loadCalendar();
}

function isLeaveDay(l_day){
    var is_leave_day = $.grep(approved_leave_days, function( a_day ) {
        return a_day.start === l_day;
    });
    return is_leave_day.length > 0;
}
function getMemoList(){    
    allinvestments = [];
    $('.loaderwrapper').fadeIn();
    RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=*,AttachmentFiles,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_',
    	function(d){
            $.each(d.results,function(i,j){            
		        allinvestments.push({
		            refcode: j.Title,
		            title: j.Title0,
		            id: j.Id,
		            im1: getIM(j.IM_x0028_1_x0029_Id),
		            im2: getIM(j.IM_x0028_2_x0029_Id),
		            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
		            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
		            pillar: j.Pillar,
		            state: j.Federal_x0020_State,
		            district: j.Districts,
		            proposal: j.Source_x0020_of_x0020_Proposal,
		            output2: j.Output2,
		            output3: j.Output3,
		            output: j.Output,
		            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
		            inv_type: j.Investee_x0020_Type,
		            proc_type: j.Proc_x002e__x0020_Type,
		            proc_cycle: j.Proc_x002e__x0020_Cycle,
		            prob_stat: j.Problem_x0020_Statement,
		            just: j.Justification,
		            rec: j.Recommendation,
		            approval_status:j.Approval_x0020_Status,
		            attachments:getAttachmentLinks(j.AttachmentFiles),
		            comments:j.Comments
		        });		                 
		    });
             populateMemo();  
        });    
}

function editMemo(){
    $('#tab-2 select').prop('disabled',false).trigger('chosen:updated');
    $('.bbs_case_table .read').hide();
    $('.bbs_case_table .edit').show();
    $('#tab-2 input,#tab-2 textarea ').prop('disabled',false);
} 

function populateMemo(){
    $('.loaderwrapper').fadeOut();    
    var s='',cancelct=0,complct=0,pendingct=0;
    $.each(allinvestments, function (i, j) {        
         s += '<tr><td>'+j.refcode+'</td><td>'+j.title+'</td><td>'+j.district+'</td><td>'+j.state+'</td><td>'+j.im1+'</td><td>'+j.im2+'</td><td>'+j.approval_status+'</td><td><a href="#" data-id="'+j.id+'" class="btn';

        if(j.approval_status=='Completed'){  
            s +=' btn-default">View</a></td></tr>';             
            complct++;
        }else if(j.approval_status=='Rejected'){
            s +=' btn-default">View</a></td></tr>';
            cancelct++;
        }else if(j.approval_status=='Pending'){
            s +=' btn-orange">Review</a></td></tr>';
            pendingct++;
        }else{
            s +=' btn-default">View</a></td></tr>';
        }             
    });
    $('#pending .btn-orange span').text('('+pendingct+')');
    $('#compvared .btn-orange span').text('('+complct+')');
    $('#approved .btn-orange span').text('('+investments.length+')');
    $('#cancelled .btn-orange span').text('('+cancelct+')');
    $('#tbcase>tbody').html(s);
    tbcase=null;
    tbcase= $('#tbcase').DataTable({responsive: true});

    $('body').on('click','#tbcase a',function(){
        var id=$(this).data('id');
        trform= getInvestment(id)[0]; 
        $('.bbs-ref').text(trform.refcode);
        $('#bbs-title').val(trform.title);
        $('#bbs-title1').text(trform.title);
        $('#state').val(trform.state);
        $('#district').val(trform.district);
        $('#primaryIM').val(trform.im1_id);
        $('#secondaryIM').val(trform.im2_id);
        $('#proposalSrc').val(trform.proposal);
        $('#output3').val(trform.output3);
        $('#output2').val(trform.output2);
        $('#output').val(trform.output);
        $('#fundceiling').val(addCommas(trform.est_fund));
        $('#inv_type').val(trform.inv_type);
        $('input[name=procurement_type][value=\'' + trform.proc_type + '\']').prop('checked', true);
        $('#proc_cycle').val(trform.proc_cycle);
        $('#hiddentext').html(trform.prob_stat);
        var prob_stat =$('#hiddentext').text();
        $('#hiddentext').html('');
        $('#prob_stat').html(prob_stat);
        $('#hiddentext').html(trform.just);
        var just=$('#hiddentext').text();
        $('#hiddentext').html('');
        $('#just').html(just);
        $('#hiddentext').html(trform.rec);      
        var rec =$('#hiddentext').text();
        $('#rec').html(rec);
        $('#hiddentext').html('');    
        $('#hiddentext').html(trform.comments);
        var c = $('#hiddentext').text();
        $('#comment').html(c);
        $('#hiddentext').html('');    
        $('#attachments').html(trform.attachments);
        $('.bbs_case_table select').chosen();
        $('[data-tab=\'tab-2\']').removeClass('hidden').trigger('click'); 
        $('.bbs_case_table textarea').each(function() {
            $(this).height($(this).prop('scrollHeight'));
        });            
    });
}

function getInvestment(id){
    var i =  $.grep(allinvestments, function (e, index) {
        return e.id === id;
    });
    if(i.length === 1) return i;    
    else return null;    
}

function submitApproveMemo(){
    var item = {'__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },'Approval_x0020_Status':'Approved','Comments':$('#comment').html()};
    updateJson(psitelst+'(\'Business Case Memo\')/items('+trform.id+')', item, success);
    function success(data) {                        
        swal('success','Business Case Memo approved successfully','success');
        var i = getInvId;
        allinvestments[i].comment= $('#comment').val();
        allinvestments[i].approval_status ='Approved';
        $('[data-tab=\'tab-1\']').trigger('click');
        $('[data-tab=\'tab-2\']').addClass('hidden');
    } 
}


function submitRejectMemo(){
    var item = {'__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },'Approval_x0020_Status':'Rejected','Comments':$('#comment').val()};  
    updateJson(psitelst+'(\'Business Case Memo\')/items('+trform.id+')', item, success);
    function success(data) {                        
        swal('success','Business Case Memo rejected successfully','success');  
        var i = getInvId;
        allinvestments[i].comment= $('#comment').val();
        allinvestments[i].approval_status ='Rejected';
        $('[data-tab=\'tab-1\']').trigger('click');
        $('[data-tab=\'tab-2\']').addClass('hidden');
    } 
}


function saveBMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to submit changes to the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitBMemo();
    });
}

function submitBMemo(){
 var item = {
            '__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
            'Title': $('.bbs-ref').text(),
            'Federal_x0020_State': $('#state').val(),
            'Districts':$('#district').val().join(', '),            
            'Source_x0020_of_x0020_Proposal':$('#proposalSrc').val(),
            //"Objective": app.trform.objective,
            'Output': $('#output').val(),
            'Output2': $('#output2').val(),
            'Output3': $('#output3').val(),
            'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
            'Investee_x0020_Type':$('#inv_type').val(),
            'Proc_x002e__x0020_Cycle': $('#proc_cycle').val(),
            'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
            'Problem_x0020_Statement':$('#prob_stat').html(),
            'Justification':$('#just').val(),
            'Recommendation':$('#rec').html(),
            'IM_x0028_1_x0029_Id': parseInt($('#primaryIM').val()),
            'IM_x0028_2_x0029_Id': parseInt($('#secondaryIM').val()),
            'Comments':$('#comment').val()          
        };
    updateJson(psitelst+'(\'Business Case Memo\')/items('+trform.id+')', item, success);
    function success(data) {                        
        swal('success','Business Case Memo updated successfully','success');  
        var i = getInvId;
        allinvestments[i].refcode= $('.bbs-ref').text();
        allinvestments[i].state= $('#state').val();
        allinvestments[i].district=('#district').val();            
        allinvestments[i].proposal=$('#proposalSrc').val();
            //"Objective": app.trform.objective;
        allinvestments[i].output= $('#output').val();
        allinvestments[i].output2= $('#output2').val();
        allinvestments[i].output3= $('#output3').val();
        allinvestments[i].est_fund= $('#fundceiling').val();
        allinvestments[i].inv_type=$('#inv_type').val();
        allinvestments[i].proc_cycle= $('#proc_cycle').val();
        allinvestments[i].proc_type= $('.procurement_type:checked').val();
        allinvestments[i].prob_stat=$('#prob_stat').html();
        allinvestments[i].just=$('#just').val();
        allinvestments[i].rec=$('#rec').html();
        allinvestments[i].im1_id= parseInt($('#primaryIM').val());
        allinvestments[i].im2_id= parseInt($('#secondaryIM').val());
        allinvestments[i].im1=getIM($('#primaryIM').val());
        allinvestments[i].im2=getIM($('#secondaryIM').val());
        allinvestments[i].comment= $('#comment').val();
        allinvestments[i].approval_status ='Comments';         
        $('[data-tab=\'tab-1\']').trigger('click');
        $('[data-tab=\'tab-2\']').addClass('hidden');
    }     
}

function saveRejectMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to reject the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitRejectMemo();
    });
}


function saveApproveMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to Approve the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitApproveMemo();
    });
}


getInvId = $.map(allinvestments, function(o, i) {
    if(o.id == trform.id) {
        return i;
    }
})[0];


function processInvestment(refcode){
    $('.loaderwrapper').fadeOut();
    loadTabs();
    var inv =  $.grep(investments, function (element, index) {
        return element.refcode === refcode;
    })[0];

    current_inv = inv;

    $('.bbs-ref').html(inv.refcode);
    $('.bbs-title').html(inv.title);
    $('.bbs-state').html(inv.state);
    $('.bbs-districts').html(inv.district);
    $('.bbs-im1').html(inv.im1);
    $('.bbs-im2').html(inv.im2);
    $('.bbs-pillar').html(inv.pillar);
    $('.bbs-prop').html(inv.proposal);
    $('.bbs-output2').html(inv.output2);
    $('.bbs-output3').html(inv.output3);
    $('.bbs-output').html(inv.output);
    $('.bbs-fund').html(inv.est_fund);
    $('.bbs-inv_type').html(inv.inv_type);
    $('.bbs-proc_type').html(inv.proc_type);
    $('.bbs-proc_cycle').html(inv.proc_cycle);
    $('.bbs-prob').html(inv.prob_stat);
    $('.bbs-just').html(inv.just);
    $('.bbs-rec').html(inv.rec);

    RestCalls('lists/GetByTitle(\'Decision Memo\')/items?$filter=Ref_x002e_Code eq \''+inv.refcode+'\'',function(d){
        if(d.results.length === 0){
            $('.btn-create-memo').removeClass('hidden');
        }
        else{
            $('.btn-create-memo').addClass('hidden');
        }        
    });   

    fixIframe();
}



function getDecisionMemoForm(){
    $('.bbs-ref').html(current_inv.refcode);
    $('.bbs-title').html(current_inv.title);
    $('.bbs-pillar').html(current_inv.pillar);
    $('.bbs-state').html(current_inv.state);
    $('.bbs-districts').html(current_inv.district);
    $('.bbs-prop').html(current_inv.proposal);
    $('.bbs-obj').html(current_inv.objective);
    $('.bbs-out').html(current_inv.output);
    $('.bbs-fund').val(current_inv.est_fund);
    $('.bbs-inv_type').html(current_inv.inv_type);

    $('.btn-next-second-form').click(function () {
        $('.first-form,.third-form, .fourth-form').hide();
        $('.second-form').fadeIn();
    });

    $('.btn-prev-first-form').click(function () {
        $('.second-form,.third-form, .fourth-form').hide();
        $('.first-form').fadeIn();
    });

    $('.btn-next-third-form').click(function () {
        $('.first-form,.second-form, .fourth-form').hide();
        $('.third-form').fadeIn();
    });

    $('.btn-prev-second-form').click(function () {
        $('.first-form,.third-form, .fourth-form').hide();
        $('.second-form').fadeIn();
    });


    $('.btn-next-fourth-form').click(function () {
        $('.first-form,.second-form,.third-form').hide();
        $('.fourth-form').fadeIn();
    });

    $('.btn-prev-third-form').click(function () {
        $('.second-form,.third-form, .fourth-form').hide();
        $('.third-form').fadeIn();
    });

    $('select').chosen();

    $('#fundceiling').bind('textchange', function (event, previousText) {
        var value = removeCommas($(this).val());
        $(this).val(addCommas(value));
    });


    $('input.upload').on('change',function(){
        var path = $(this).val();
        var filename = path.substr(path.lastIndexOf('\\') + 1);
        $(this).closest('.input-group').find('.inputFiles').val(filename);
    });

    $('body').on('click','.save-dec-memo',function () {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        saveDecMemo();
    });

    $('#decision-memo-form').modal('show');
}


function saveDecMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to submit the decision case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitDecMemo();
    });
}

function submitDecMemo(){
    var item = {
        '__metadata': { 'type': 'SP.Data.Decision_x0020_MemoListItem' },
        'Title': current_inv.title,
        'Ref_x002e_Code': current_inv.refcode,
        'Federal_x0020_State': current_inv.state,
        'District':current_inv.district,
        'Source_x0020_of_x0020_proposal':current_inv.proposal,
        'Objective': current_inv.objective,
        'Output': current_inv.output,
        'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
        'Investee_x0020_Type': current_inv.inv_type,
        'Start_x0020_Date': moment($('#startdate').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'End_x0020_Date': moment($('#enddate').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'Email':$('#email').val(),
        'Phone':$('#phone').val(),
        'Address':$('#address').val(),
        'Background': $('#background').val(),
        'Key_x0020_Activities': $('#keyact').val(),
        'Value_x0020_for_x0020_money': $('#valuemoney').val(),
        'Risk_x0020_Summary': $('#risksummary').val(),
        'Sustainability': $('#sustain').val(),
        'Conflict_x0020_Sensitivity': $('#conflictsens').val(),
        'Recommendation': $('#recomm').val(),
        'IM_x0028_1_x0029_Id': current_inv.im1_id,
        'IM_x0028_2_x0029_Id': current_inv.im2_id
    };

    postJson(psitelst + '(\'Decision Memo\')/items', item, success);
    function success(d){
        $('#decision-memo-form').modal('hide');
        $('.btn-create-memo').addClass('hidden');
        swal('Success','Decision Memo created successfully','success');
        checkDigestTimeout();
    }
}




/*!
 * jQuery TextChange Plugin
 * http://www.zurb.com/playground/jquery-text-change-custom-event
 *
 * Copyright 2010, ZURB
 * Released under the MIT License
 */
 (function(a){a.event.special.textchange={setup:function(){a(this).data('lastValue',this.contentEditable==='true'?a(this).html():a(this).val());a(this).bind('keyup.textchange',a.event.special.textchange.handler);a(this).bind('cut.textchange paste.textchange input.textchange',a.event.special.textchange.delayedHandler)},teardown:function(){a(this).unbind('.textchange')},handler:function(){a.event.special.textchange.triggerIfChanged(a(this))},delayedHandler:function(){var c=a(this);setTimeout(function(){a.event.special.textchange.triggerIfChanged(c)},
 25)},triggerIfChanged:function(a){var b=a[0].contentEditable==='true'?a.html():a.val();b!==a.data('lastValue')&&(a.trigger('textchange',[a.data('lastValue')]),a.data('lastValue',b))}};a.event.special.hastext={setup:function(){a(this).bind('textchange',a.event.special.hastext.handler)},teardown:function(){a(this).unbind('textchange',a.event.special.hastext.handler)},handler:function(c,b){b===''&&b!==a(this).val()&&a(this).trigger('hastext')}};a.event.special.notext={setup:function(){a(this).bind('textchange',
 a.event.special.notext.handler)},teardown:function(){a(this).unbind('textchange',a.event.special.notext.handler)},handler:function(c,b){a(this).val()===''&&a(this).val()!==b&&a(this).trigger('notext')}}})(jQuery);
function loadbatchTaskcount(){
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst+'(\'Decision Memo\')/items?$select=Id&$filter=(Status eq \'Pending\')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getDecCount' });
    batchRequest = new BatchRequest();    
    batchRequest.endpoint = psitelst+'(\'Business Case Memo\')/items?$select=Id&$filter=(Status eq \'Pending\')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMemoCount' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = sitelst+'(\'Modification Memo\')/items?$select=Id';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getModMemoCount' });    
    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });            
            if (command[0].title == 'getDecCount') {
                getDecCount(v.result.result.value);
            }else if (command[0].title == 'getMemoCount') {
                getMemoCount(v.result.result.value);
            }
            else if (command[0].title == 'getModMemoCount') {
                getModMemoCount(v.result.result.value);
            }           
        });              
    }).fail(function (err) {
        onError(err);
    });
}

function getDecCount(d){
    $('#decmemo .info h4').text(d.length);
}

function getMemoCount(d){
    $('.loaderwrapper').fadeOut(); 
    $('#bcmemo .info h4').text(d.length);
}

function getModMemoCount(d){
    $('.loaderwrapper').fadeOut(); 
    $('#modmemo .info h4').text(d.length);
}

function loadBMemoTasks(){
    $('.loaderwrapper').fadeIn();
    if(getCache('bmtasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/bmemo.html',function(response, status, xhr) {
            cache('bmtasks_html', response);
            processBForm();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('bmtasks_html'));
        processBForm();
    }
}

function processBForm(){ 
    loadTabs();  
    getMemoList(); 
    readyForms();   
}

function readyForms(){
    $(ims).each(function(index,value){
        $('#primaryIM,#secondaryIM').append('<option value="'+value.id+'">'+value.name+'</option>');
    });   

    $('select#state').change(function() {
        var state = $(this);
        var selected_districts = $.grep(districts, function(element, index) {
            if ($(state).find('option:selected').attr('data-initial') === 'MR') {
                return true;
            } else {
                return element.state === $(state).find('option:selected').attr('data-initial');
            }
        });
        $('select#district').empty();
        var content = '';
        for (var i = 0; i < selected_districts.length; i++) {
            content += '<option value="' + selected_districts[i].name + '">' + selected_districts[i].name + '</option>';
        }
        $('#district').append(content).trigger('chosen:updated');
    });   

    $('#state,#pillar').change(function() {
        getReference();
    });    
}

function loadDecMemoTasks(){
    $('.loaderwrapper').fadeIn();
    if(getCache('dmtasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/dmemo.html',function(response, status, xhr) {
            cache('dmtasks_html', response);
            processDForm();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('dmtasks_html'));
        processDForm();
    }
}

function processDForm(){ 
    loadTabs();  
    getDecMemoList(); 
    readyForms();   
}

function loadModMemoTasks(){
    $('.loaderwrapper').fadeIn();
    if(getCache('mmtasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/modmemo.html',function(response, status, xhr) {
            cache('mmtasks_html', response);
            processModForm();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('mmtasks_html'));
        processModForm();
    }
}

function processModForm(){ 
    //#TODO
    loadTabs();  
    getDecMemoList(); 
    readyForms();   
}

