var approver=false,user={access:''},investments=[],users = [],ims=[],locations=[],focals=[],draftmemos=[];
var siteUrl=_spPageContextInfo.webAbsoluteUrl+'/_api/web/';
var psitelst=siteUrl+'lists/GetByTitle';

$(document).ready(function() {
    preloadbatch();
    loadTabs();
});
function preloadbatch(){
    RestCalls('lists/GetByTitle(\'Approvers List\')/items?$select=Title&$filter=UserId eq '+ _spPageContextInfo.userId,function(d){
        $.each(d.results,function(i,j){
            if(j.Title!=null){
                approver=true;
                user.access=j.Title;
            }
        });
        if(approver) $('.menu-tasks').removeClass('hidden'); else $('.menu-tasks').addClass('hidden');
        loadbatch();
    });         
}

function loadDraftMemos(){
    var s='';
    $.each(draftmemos, function (i, j) {        
         s += '<tr><td>'+j.title+'</td><td>'+j.district+'</td><td>'+j.state+'</td><td>'+j.im1+'</td><td>'+j.im2+'</td><td><a href="#" data-id="'+j.id+'" class="btn btn-orange">Review</a></td></tr>';
    });
    var tbdraftmemo=null;
    $('#tbdraftmemo>tbody').html(s);    
    tbdraftmemo= $('#tbdraftmemo').DataTable({responsive: true});
    $('body').on('click','#tbdraftmemo a',function(){ loadDraftMemo($(this).data('id')); });
}


function loadbatch(){
    var bcmemurl=psitelst+'(\'Business Case Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=(Approval_x0020_Status eq \'Approved\') and (IM_x0028_1_x0029_Id eq '+
    _spPageContextInfo.userId+' or IM_x0028_2_x0029_Id eq '+_spPageContextInfo.userId+')';
    if(user.access =='All' || user.access =='Team Leader' || user.access =='Senior IM'|| user.access =='SO'|| user.access =='KMCU'){
        bcmemurl=psitelst+'(\'Business Case Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=Approval_x0020_Status eq \'Approved\'';
    }
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(49)/users';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata'};
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getIMs' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(8)/users';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getUsers' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Focal Person\')/items?$select=User/Id,User/Title&$expand=User';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getFocals' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst+'(\'Business Case Memo\')/items?$select=*,AttachmentFiles,AttachmentFiles/ServerRelativeUrl,AttachmentFiles/FileName,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_&$filter=(Status eq \'Draft\') and (IM_x0028_1_x0029_Id eq '+
    _spPageContextInfo.userId+' or IM_x0028_2_x0029_Id eq '+_spPageContextInfo.userId+')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMyDraftMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'TR Locations\')/items?$select=Title';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getLocations' });
    batchRequest = new BatchRequest();

    batchRequest.endpoint = bcmemurl;
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
    
    
    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });            
            if (command[0].title == 'getIMs') {
                getIMs(v.result.result.value);
            }else if (command[0].title == 'getUsers') {
                getUsers(v.result.result.value);
            }else if (command[0].title == 'getMyDraftMemos') {
                getMyDraftMemos(v.result.result.value);
            }else if (command[0].title == 'getFocals') {
                getFocals(v.result.result.value);
            } else if (command[0].title == 'getLocations') {
                getLocations(v.result.result.value);
            }else if (command[0].title == 'getAprInvestments') {
                getAprInvestments(v.result.result.value);
            }         
        });     
    }).fail(function (err) {
        onError(err);
    });
}

function getMyDraftMemos(d){
    draftmemos=[];
    $.each(d,function(i,j){
        draftmemos.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status:j.Approval_x0020_Status,
            attachments:getAttachmentLinks(j.AttachmentFiles),
            comments:j.Comments,
            socomments:j.SO_x0020_Comments,
            tlcomments:j.TL_x0020_Comments,
        });
    });
    loadDraftMemos();
}

function loadDraftMemos(){
    var s='';
    $.each(draftmemos, function (i, j) {        
         s += '<tr><td>'+j.title+'</td><td>'+j.district+'</td><td>'+j.state+'</td><td>'+j.im1+'</td><td>'+j.im2+'</td><td><a href="#" data-id="'+j.id+'" class="btn btn-orange">Review</a></td></tr>';
    });
    var tbdraftmemo=null;
    $('#tbdraftmemo>tbody').html(s);    
    tbdraftmemo= $('#tbdraftmemo').DataTable({responsive: true});
    $('body').on('click','#tbdraftmemo a',function(){ loadDraftMemo($(this).data('id')); });
}

function getUsers(d){
    $.each(d,function(i,j){
        users.push({name: j.Title,id:j.Id});
    });
}

function getFocals(d){
    $.each(d,function(i,j){
        focals.push({name: j.User.Title,id:j.User.Id});
    });
}

function getIMs(d){
    ims=[];
    $.each(d, function (i, j){
        ims.push({id:j.Id,name:j.Title});
    });    
}

function getLocations(d){
    locations=[];
    $.each(d, function (i, j){
        locations.push(j.Title);
    });
}


function getAprInvestments(d){
    investments=[];
    $.each(d,function(i,j){   
        investments.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status:j.Approval_x0020_Status,
            attachments:getAttachmentLinks(j.AttachmentFiles),
            comments:j.Comments,
            socomments:j.SO_x0020_Comments,
            tlcomments:j.TL_x0020_Comments,
        });

    });
    loadInvestments();
}



function loadInvestments(){
    var inv_content = '';
    for(i=0;i<investments.length;i++){
        var inv = investments[i];
        if(i%3 === 0){
            inv_content += '<div class="row">';
        }
        inv_content += '<div class="col-md-4">';
        inv_content += '<div class="box-height" onclick="loadInvestment(\''+inv.refcode+'\')">';
        inv_content += '<div class="col-md-4 h-full div-counter">';
        inv_content += '<p class="p-count">'+pad((i+1)+'',2)+'</p>';
        inv_content += '</div>';
        inv_content += '<div class="col-md-8 h-full div-inv">';
        inv_content += '<p class="p-ref mb5">'+inv.refcode+'</p>';
        inv_content += '<p class="p-title mb5">'+inv.title+'</p>';
        inv_content += '<p class="p-im mb5"><b>IM(1):</b>   '+inv.im1+'</p><p class="m0 p-im"><b>IM(2):</b>   '+inv.im2+'</p></p>';
        inv_content += '</div>';
        inv_content += '</div>';
        inv_content += '</div>';
        if((i - 2) % 3 === 0){
            inv_content += '</div>';
        }
        else if(investments.length - 1 === i){
            inv_content += '</div>';
        }
    }
    $('.loaderwrapper').fadeOut();
    $('#inv-contain').empty().append(inv_content);
}


function getMemoForm(){
    $('select').chosen();
    $('#primaryIM').empty().append('<option value="'+_spPageContextInfo.userId+'">'+_spPageContextInfo.userDisplayName+'</option>');

    $('#secondaryIM').empty();
    $(ims).each(function(index,value){
        $('#secondaryIM').append('<option value="'+value.id+'">'+value.name+'</option>');
    });
    $('#memo-form .modal-title').html('New Business Case Memo'); 
    $('#memo-form .save-memo').show();
    $('#memo-form .update-memo').hide();

    $('#primaryIM,#secondaryIM').trigger('chosen:updated');

    $('select#state').change(function() {
        var state = $(this);
        var selected_districts = $.grep(districts, function(element, index) {
            if ($(state).find('option:selected').attr('data-initial') === 'MR') {
                return true;
            } else {
                return element.state === $(state).find('option:selected').attr('data-initial');
            }
        });
        $('select#district').empty();
        var content = '';
        for (var i = 0; i < selected_districts.length; i++) {
            content += '<option value="' + selected_districts[i].name + '">' + selected_districts[i].name + '</option>';
        }
        $('#district').append(content).trigger('chosen:updated');
    });

    $('#nextbcmemo').click(function() {
        $('#bcmemo').hide();
        $('#bcmemonext').removeClass('hidden').fadeIn();
    });
    $('#bcprevmemo').click(function() {
        $('#bcmemonext').hide();
        $('#bcmemo').removeClass('hidden').fadeIn();
    });
    $('.save-memo-draft').click(function() {
        saveMemoDraft();
    });
    $('.save-memo').click(function() {
        saveMemo();
    });

    $('#state').change(function() {
        getReference();
    });
    $('#pillar').change(function() {
        getReference();
    });

    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    $('#fundceiling').bind('textchange', function(event, previousText) {
        var value = removeCommas($(this).val());
        if ($.isNumeric(value)) {
            $(this).val(addCommas(value));
        } else {
            $(this).val('');
        }
    });
    $('input.upload').on('change', function() {
        var path = $(this).val();
        var filename = path.substr(path.lastIndexOf('\\') + 1);
        $(this).closest('.input-group').find('.inputFiles').val(filename);
    });

    $('#memo-form').modal('show');
}


function saveMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to submit business case memo for review and approval?',
        type: 'warning',
        showCancelButton: true,
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Final');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitMemo();
    });
}

function saveMemoDraft() {
    swal({
        title: 'Are you sure?',
        text: '<p>Only the primary and secondary im can view</p>',
        html: true,
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'Save as Draft',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Draft');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitMemo();
    });
}

function submitMemo(){
    var item = {
        '__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
        'Title': $('#refno-h').val(),
        'Pillar': $('#pillar').val(),
        'Title0':$('#bCasetitle').val(),
        'Federal_x0020_State': $('#state').val(),
        'Districts':$('#district').val().join(', '),            
        'Source_x0020_of_x0020_Proposal':$('#proposalSrc').val(),
        'Output2': $('#output2').val(),
        'Output3': $('#output3').val(),
        'Output': $('#output').val(),
        'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
        'Investee_x0020_Type': $('#invtype').val(),
        'Proc_x002e__x0020_Cycle': $('#procurement_cycle').val(),
        'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
        'Problem_x0020_Statement':$('#probstat').val(),
        'Justification':$('#justification').val(),
        'Recommendation':$('#recomhead').val(),
        'IM_x0028_1_x0029_Id': parseInt($('#primaryIM').val()),
        'IM_x0028_2_x0029_Id': parseInt($('#secondaryIM').val()),
        'Status': $('#submit-type').val(),
    };

    postJson(psitelst + '(\'Business Case Memo\')/items', item, success);
    function success(d){
        $('#memo-form').modal('hide');
        swal('Success','Business Memo created successfully','success');
        checkDigestTimeout();
    }

}

function loadInvestment(refcode){
    $('.loaderwrapper').fadeIn();
    if(getCache('inv_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/investment.html',function(response, status, xhr) {
            cache('inv_html', response);
            processInvestment(refcode);
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('inv_html'));
        processInvestment(refcode);
    }
}

function updateMemo(){
    swal({
        title: 'Are you sure?',
        text: 'You want to submit business case memo for review and approval?',
        type: 'warning',
        showCancelButton: true,
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Final');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitUpdateMemo();
    });
}

function submitUpdateMemo(){
    var item = {
        '__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
        'Title': $('#refno-h').val(),
        'Pillar': $('#pillar').val(),
        'Title0':$('#bCasetitle').val(),
        'Federal_x0020_State': $('#state').val(),
        'Districts':$('#district').val().join(', '),            
        'Source_x0020_of_x0020_Proposal':$('#proposalSrc').val(),
        'Output2': $('#output2').val(),
        'Output3': $('#output3').val(),
        'Output': $('#output').val(),
        'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
        'Investee_x0020_Type': $('#invtype').val(),
        'Proc_x002e__x0020_Cycle': $('#procurement_cycle').val(),
        'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
        'Problem_x0020_Statement':$('#probstat').val(),
        'Justification':$('#justification').val(),
        'Recommendation':$('#recomhead').val(),
        'IM_x0028_1_x0029_Id': parseInt($('#primaryIM').val()),
        'IM_x0028_2_x0029_Id': parseInt($('#secondaryIM').val()),
        'Status': $('#submit-type').val(),
    };

    updateJson(psitelst + '(\'Business Case Memo\')/items('+$('#bmemoid').val()+')', item, success);
    function success(d){
        $('#memo-form').modal('hide');
        swal('Success','Business Memo updated successfully','success');
        location.reload();
    }

}


function getReference() {
    var c_pillar_val = $('#pillar').val();
    var c_state_val = $('#state').val();
    if (c_state_val && c_pillar_val) {
        RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=Id&$filter=Pillar eq \''+encodeURIComponent(c_pillar_val)+'\'&Federal_x0020_State eq \''+encodeURIComponent(c_state_val)+'\'',function(d){
            var id = pad((d.results.length + 1) + '', 2);
            var ref = $('#pillar option:selected').attr('data-initial') + '-' + $('#state option:selected').attr('data-initial') + '-' + id;
            $('#refno').text(ref);
            $('#refno-h').val(ref);
        }); 
    }
}

function loadDraftMemo(id){
    var d =  $.grep(draftmemos, function (element, index) {
        return element.id === id;
    });

    console.log(d);
    if(d.length< 1) return;
    d=d[0];
    getMemoForm();
    $('.update-memo').click(function() {
        updateMemo();
    });
    $('#refno').text(d.refcode);
    $('#refno-h').val(d.refcode);
    $('#bmemoid').val(id);
    $('#bCasetitle').val(d.title);
    $('#state').val(d.state.split(','));
    $('#district').val(d.district.split(','));
    $('#output').val(d.output);
    $('#output2').val(d.output2);
    $('#output3').val(d.output3);
    $('#pillar').val(d.pillar);
    $('#fundceiling').val(d.est_fund);
    $('#invtype').val(d.inv_type);
    $('#primaryIM').val(d.im1_id);
    $('#secondaryIM').val(d.im2_id);
    $('#procurement_cycle').val(d.proc_cycle);
    $('#hiddentext').html(d.prob_stat);
    var prob_stat=$('#hiddentext').text();
    $('#hiddentext').html('');
    $('#probstat').html(prob_stat);
    $('#hiddentext').html(d.just);
    var just=$('#hiddentext').text();
    $('#hiddentext').html('');
    $('#justification').html(just);
    $('#hiddentext').html(d.rec);      
    var rec =$('#hiddentext').text();
    $('#rec').html(rec);
    $('#hiddentext').html(''); 
    $('#memo-form select').trigger('chosen:updated');
    $('#memo-form .modal-title').html('Edit Business Case Memo'); 
    $('#memo-form .save-memo').hide();
    $('#memo-form .update-memo').show();
}