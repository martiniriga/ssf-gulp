function processHome(){
    loadTabs();
    getMemoCards();
}

function loadHome(){
    $('.loaderwrapper').fadeIn();
    if(getCache('home_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/home.html',function(response, status, xhr) {
            cache('home_html', response);
            processHome();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('home_html'));
        processHome();
    }
}

function loadForms(){
    $('.loaderwrapper').fadeIn();
    if(getCache('forms_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/forms.html',function(response, status, xhr) {
            cache('forms_html', response);
            processForms();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('forms_html'));
        processForms();
    }
}

function processForms(){
    loadTabs();
    $('.loaderwrapper').fadeOut();
    $(users).each(function(index,value){
        if(value.id !== _spPageContextInfo.userId){
            $('#supervisor').append('<option value="'+value.id+'">'+value.name+'</option>');
        }
    });

    $(focals).each(function(index,value){
        $('#focal').append('<option value="'+value.id+'">'+value.name+'</option>');
    });

    $(locations).each(function(i,v){
        $('select.dest').append('<option>'+v+'</option>');
    });

    var approved_leave_days_current_month = $.grep(approved_leave_days, function( a_day ) {
        return moment(a_day.start, 'YYYY-MM-DD').format('M') === moment().format('M');
    });

    $('.days-leave-taken-current-month').text(pad(approved_leave_days_current_month.length+'',2));
    $('.days-leave-taken').text(pad(approved_leave_days.length+'',2));

    $('.btn-submit-leave').click(function () {
        $('#leave-form').data('formValidation').validate();
        if($('#leave-form').data('formValidation').isValid()){
            if(leave_days.length > 0){
                saveLeave();
            }
            else{
                swal('Error!', 'You must select atleast one day on the calendar', 'error');
            }
        }
    });

    $('.btn-submit-travel').click(function () {
        $('#travel-form').data('formValidation').validate();
        var fv = $('#travel-form').data('formValidation');
        if($('#dest3').val()){
            fv.enableFieldValidators('date3', true).revalidateField('date3');
        }
        else{
            fv.enableFieldValidators('date3', false).revalidateField('date3');
        }
        if($('#dest4').val()){
            fv.enableFieldValidators('date4', true).revalidateField('date4');
        }
        else{
            fv.enableFieldValidators('date4', false).revalidateField('date4');
        }

        if($('#travel-form').data('formValidation').isValid()){
            saveTravel();
        }
    });

    validateLeaveForm();
    validateTravelForm();

    $('select').chosen({
        allow_single_deselect: true
    });
    

    $('#datetimepicker').datetimepicker({                         
        format: 'DD/MM/YYYY',                           
        minDate: moment().startOf('day').add(5, 'days')
    }).on('dp.change', function(e) {                            
        $('#datetimepicker1').data('DateTimePicker').minDate(e.date);
        $('#datetimepicker2').data('DateTimePicker').minDate(e.date);
        $('#datetimepicker3').data('DateTimePicker').minDate(e.date);
        $('#travel-form').formValidation('revalidateField', 'date1')
        .formValidation('revalidateField', 'date2')
        .formValidation('revalidateField', 'date3')
        .formValidation('revalidateField', 'date4');
    });
    $('#datetimepicker1,#datetimepicker2,#datetimepicker3').datetimepicker({
        format: 'DD/MM/YYYY',                           
        useCurrent: false,
        minDate: moment().startOf('day').add(5, 'days')
    }).on('dp.change dp.show', function(e) { 
        $('#travel-form').formValidation('revalidateField', 'date1')
        .formValidation('revalidateField', 'date2')
        .formValidation('revalidateField', 'date3')
        .formValidation('revalidateField', 'date4');
    });
    loadCalendar();
}


function loadTasks(){
    $('.loaderwrapper').fadeIn();
    if(getCache('tasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/tasks.html',function(response, status, xhr) {
            cache('tasks_html', response);
            processTasks();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('tasks_html'));
        processTasks();
    }
}

function processTasks(){    
    loadTabs();
    loadbatchTaskcount(); 
}


function getReference() {
    var c_pillar_val = $('#pillar').val();
    var c_state_val = $('#state').val();
    if (c_state_val && c_pillar_val) {
        RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=Id&$filter=Pillar eq \''+encodeURIComponent(c_pillar_val)+'\'&Federal_x0020_State eq \''+encodeURIComponent(c_state_val)+'\'',function(d){
            var id = pad((d.results.length + 1) + '', 2);
            var ref = $('#pillar option:selected').attr('data-initial') + '-' + $('#state option:selected').attr('data-initial') + '-' + id;
            $('#refno').text(ref);
            $('#refno-h').val(ref);
        }); 
    }
}

