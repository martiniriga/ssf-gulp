
function getDecisionMemoForm(){
    $('.bbs-ref').html(current_inv.refcode);
    $('.bbs-title').html(current_inv.title);
    $('.bbs-pillar').html(current_inv.pillar);
    $('.bbs-state').html(current_inv.state);
    $('.bbs-districts').html(current_inv.district);
    $('.bbs-prop').html(current_inv.proposal);
    $('.bbs-obj').html(current_inv.objective);
    $('.bbs-out').html(current_inv.output);
    $('.bbs-fund').val(current_inv.est_fund);
    $('.bbs-inv_type').html(current_inv.inv_type);

    $('.date').datetimepicker({format: 'DD/MM/YYYY'});
  
    $('.btn-next-second-form').click(function () {
        $('.first-form,.third-form, .fourth-form').hide();
        $('.second-form').fadeIn();
    });

    $('.btn-prev-first-form').click(function () {
        $('.second-form,.third-form, .fourth-form').hide();
        $('.first-form').fadeIn();
    });

    $('.btn-next-third-form').click(function () {
        $('.first-form,.second-form, .fourth-form').hide();
        $('.third-form').fadeIn();
    });

    $('.btn-prev-second-form').click(function () {
        $('.first-form,.third-form, .fourth-form').hide();
        $('.second-form').fadeIn();
    });


    $('.btn-next-fourth-form').click(function () {
        $('.first-form,.second-form,.third-form').hide();
        $('.fourth-form').fadeIn();
    });

    $('.btn-prev-third-form').click(function () {
        $('.second-form,.third-form, .fourth-form').hide();
        $('.third-form').fadeIn();
    });

    $('select').chosen();

    $('#fundceiling').bind('textchange', function (event, previousText) {
        var value = removeCommas($(this).val());
        $(this).val(addCommas(value));
    });


    $('input.upload').on('change',function(){
        var path = $(this).val();
        var filename = path.substr(path.lastIndexOf('\\') + 1);
        $(this).closest('.input-group').find('.inputFiles').val(filename);
    });

    $('body').on('click','.save-dec-memo',function () {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        saveDecMemo();
    });

    $('#decision-memo-form').modal('show');
}


function saveDecMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to submit the decision case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitDecMemo();
    });
}

function submitDecMemo(){
    var item = {
        '__metadata': { 'type': 'SP.Data.Decision_x0020_MemoListItem' },
        'Title': current_inv.title,
        'Ref_x002e_Code': current_inv.refcode,
        'Federal_x0020_State': current_inv.state,
        'District':current_inv.district,
        'Source_x0020_of_x0020_proposal':current_inv.proposal,
        'Objective': current_inv.objective,
        'Output': current_inv.output,
        'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
        'Investee_x0020_Type': current_inv.inv_type,
        'Start_x0020_Date': moment($('#startdate').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'End_x0020_Date': moment($('#enddate').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'Email':$('#email').val(),
        'Phone':$('#phone').val(),
        'Address':$('#address').val(),
        'Background': $('#background').val(),
        'Key_x0020_Activities': $('#keyact').val(),
        'Value_x0020_for_x0020_money': $('#valuemoney').val(),
        'Risk_x0020_Summary': $('#risksummary').val(),
        'Sustainability': $('#sustain').val(),
        'Conflict_x0020_Sensitivity': $('#conflictsens').val(),
        'Recommendation': $('#recomm').val(),
        'IM_x0028_1_x0029_Id': current_inv.im1_id,
        'IM_x0028_2_x0029_Id': current_inv.im2_id,
        'Approval_x0020_Status':'Pending',
        'Status_x0028_2_x0029_':'Pending'
    };

    postJson(psitelst + '(\'Decision Memo\')/items', item, success);
    function success(d){
        $('#decision-memo-form').modal('hide');
        $('.btn-create-memo').addClass('hidden');
        swal('Success','Decision Memo created successfully','success');
        checkDigestTimeout();
    }
}


