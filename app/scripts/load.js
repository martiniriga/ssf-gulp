
function preloadbatch(){
    RestCalls('lists/GetByTitle(\'Approvers List\')/items?$select=Title&$filter=UserId eq'+ _spPageContextInfo.userId,function(d){
        $.each(d,function(i,j){
            if(j.User.Id==_spPageContextInfo.userId){
                approver=true;
                user.access=j.Title;
            }
        });
        if(approver) $('.menu-tasks').removeClass('hidden'); else $('.menu-tasks').addClass('hidden');
    });         
}

function loadbatch(){
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(49)/users';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata'};
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getIMs' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteUrl + 'sitegroups(8)/users';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getUsers' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Focal Person\')/items?$select=User/Id,User/Title&$expand=User';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getFocals' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = mydrafturl;
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMyDraftMemos' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'TR Locations\')/items?$select=Title';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getLocations' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = memourl;
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst + '(\'Leave Requests\')/items?$filter=(Status eq \'Approved\') and (AuthorId eq '+_spPageContextInfo.userId+')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getApprovedLeave' });
    
    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });            
            if (command[0].title == 'getIMs') {
                getIMs(v.result.result.value);
            }else if (command[0].title == 'getUsers') {
                getUsers(v.result.result.value);
            }else if (command[0].title == 'getMyDraftMemos') {
                getMyDraftMemos(v.result.result.value);
            }else if (command[0].title == 'getFocals') {
                getFocals(v.result.result.value);
            } else if (command[0].title == 'getLocations') {
                getLocations(v.result.result.value);
            }else if (command[0].title == 'getAprInvestments') {
                getAprInvestments(v.result.result.value);
            }else if (command[0].title == 'getApprovedLeave') {
                getApprovedLeave(v.result.result.value);
            }          
        });     
    }).fail(function (err) {
        onError(err);
    });
}

function getMyDraftMemos(d){
    draftmemos=[];
    $.each(d,function(i,j){
        draftmemos.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status:j.Approval_x0020_Status,
            attachments:getAttachmentLinks(j.AttachmentFiles),
            comments:j.Comments,
            socomments:j.SO_x0020_Comments,
            tlcomments:j.TL_x0020_Comments,
        });
    });
    loadDraftMemos();
}

function getUsers(d){
    $.each(d,function(i,j){
        users.push({name: j.Title,id:j.Id});
    });
}

function getFocals(d){
    $.each(d,function(i,j){
        focals.push({name: j.User.Title,id:j.User.Id});
    });
}

function getIMs(d){
    ims=[];
    $.each(d, function (i, j){
        ims.push({id:j.Id,name:j.Title});
    });
}

function getLocations(d){
    locations=[];
    $.each(d, function (i, j){
        locations.push(j.Title);
    });
}


function getAprInvestments(d){
    checkDigestTimeout();
    investments=[];
    $.each(d,function(i,j){   
        investments.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status:j.Approval_x0020_Status,
            attachments:getAttachmentLinks(j.AttachmentFiles),
            comments:j.Comments,
            socomments:j.SO_x0020_Comments,
            tlcomments:j.TL_x0020_Comments,
        });

    });
    loadHome();
}


function getApprovedLeave(d){
    $.each(d,function(i,j){
        var app_days =j.Dates.split(', ');
        var app_day = null;
        $(app_days).each(function (idx,val) {
            app_day = moment(val,'DD-MM-YYYY').format('YYYY-MM-DD');
            approved_leave_days.push({
                id: app_day,
                title: 'Leave',
                allDay: true,
                start: app_day,
                className: 'hidden'
            });
        });
    });
}

$(document).ready(function() {
    $('br').remove();
    $('.menu-home').click(function(){
        loadHome();
    });
    $('.menu-forms').click(function(){
        loadForms();
    });
    $('.menu-tasks').click(function(){
        loadTasks();
    });

    $('nav ul li').click(function () {
        changeActiveMenu($(this));
    });
    loadbatch();  

    setTimeout(function(){
        checkDigestTimeout();
    }, 240000);
});