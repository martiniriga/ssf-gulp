function loadbatchTaskcount(){
    var commands = [];
    var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst+'(\'Decision Memo\')/items?$select=Id&$filter=(Status eq \'Pending\')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getDecCount' });
    batchRequest = new BatchRequest();    
    batchRequest.endpoint = psitelst+'(\'Business Case Memo\')/items?$select=Id&$filter=(Approval_x0020_Status eq \'Pending\')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMemoCount' });
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst+'(\'Modification Memo\')/items?$select=Id';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getModMemoCount' }); 
    // batchRequest = new BatchRequest();
    // batchRequest.endpoint = psitelst+'(\'Performance Management\')/items?$select=Id&$filter=(Approval_x0020_Status eq \'Pending\')';
    // batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    // commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getPerfCount' });   
    batchRequest = new BatchRequest();
    batchRequest.endpoint = psitelst+'(\'CloseOut\')/items?$select=Id&$filter=(Approval_x0020_Status eq \'Pending\')';
    batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getCloseOutCount' }); 
    batchExecutor.executeAsync().done(function (result) {
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });            
            if (command[0].title == 'getDecCount') {
                getDecCount(v.result.result.value);
            }else if (command[0].title == 'getMemoCount') {
                getMemoCount(v.result.result.value);
            }else if (command[0].title == 'getModMemoCount') {
                getModMemoCount(v.result.result.value);
            }else if (command[0].title == 'getCloseOutCount') {
                getCloseOutCount(v.result.result.value);
            }           
        });              
    }).fail(function (err) {
        onError(err);
    });
}

function getDecCount(d){
    $('#decmemo .info h4').text(d.length);
}

function getMemoCount(d){
    $('.loaderwrapper').fadeOut(); 
    $('#bcmemo .info h4').text(d.length);
}

function getModMemoCount(d){
    $('.loaderwrapper').fadeOut(); 
    $('#modmemo .info h4').text(d.length);
}

function getCloseOutCount(d){
    $('.loaderwrapper').fadeOut(); 
    $('#closeout .info h4').text(d.length);
}

// function getPerfCount(d){
//     $('.loaderwrapper').fadeOut(); 
//     $('#permgt .info h4').text(d.length);
// }

function loadBMemoTasks(){
    checkDigestTimeout();
    $('.loaderwrapper').fadeIn();
    if(getCache('bmtasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/bmemo.html',function(response, status, xhr) {
            cache('bmtasks_html', response);
            processBForm();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('bmtasks_html'));
        processBForm();
    }
}

function processBForm(){ 
    loadTabs();  
    getMemoList(); 
    readyForms();   
}

function readyForms(){
    $(ims).each(function(index,value){
        $('#primaryIM,#secondaryIM').append('<option value="'+value.id+'">'+value.name+'</option>');
    });   

    $('select#state').change(function() {
        var state = $(this);
        var selected_districts = $.grep(districts, function(element, index) {
            if ($(state).find('option:selected').attr('data-initial') === 'MR') {
                return true;
            } else {
                return element.state === $(state).find('option:selected').attr('data-initial');
            }
        });
        $('select#district').empty();
        var content = '';
        for (var i = 0; i < selected_districts.length; i++) {
            content += '<option value="' + selected_districts[i].name + '">' + selected_districts[i].name + '</option>';
        }
        $('#district').append(content).trigger('chosen:updated');
    });   

    $('#state,#pillar').change(function() {
        getReference();
    });    
}

function loadDecMemoTasks(){    
    $('.loaderwrapper').fadeIn();
    if(getCache('dmtasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/dmemo.html',function(response, status, xhr) {
            cache('dmtasks_html', response);
            processDForm();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('dmtasks_html'));
        processDForm();
    }
}

function processDForm(){ 
    loadTabs();  
    getDecMemoList(); 
    readyForms();   
}

function loadModMemoTasks(){
    $('.loaderwrapper').fadeIn();
    if(getCache('mmtasks_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/modmemo.html',function(response, status, xhr) {
            cache('mmtasks_html', response);
            processModForm();
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('mmtasks_html'));
        processModForm();
    }
}

function processModForm(){ 
    //#TODO
    loadTabs();  
    getDecMemoList(); 
    readyForms();   
}

