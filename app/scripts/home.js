function getMemoCards(){    
    if(load=='init'){
        load='loaded';
        $('.loaderwrapper').fadeOut();
        loadInvestments();
        loadDraftMemos();
    }else{
        checkDigestTimeout();
        var commands = [];
        var batchExecutor = new RestBatchExecutor(_spPageContextInfo.webAbsoluteUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
        batchRequest = new BatchRequest();
        batchRequest.endpoint = memourl;
        batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
        commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getAprInvestments' });
        batchRequest = new BatchRequest();
        batchRequest.endpoint = mydrafturl;
        batchRequest.headers = { 'accept': 'application/json;odata=nometadata' };
        commands.push({ id: batchExecutor.loadRequest(batchRequest), title: 'getMyDraftMemos' });
        batchExecutor.executeAsync().done(function (result) {
            $.each(result, function (k, v) {
                var command = $.grep(commands, function (command) {
                    return v.id === command.id;
                });            
                if (command[0].title == 'getMyDraftMemos') {
                    getMyDraftMemos(v.result.result.value);
                }else if (command[0].title == 'getAprInvestments') {
                    getHomeAprInvestments(v.result.result.value);
                }          
            });     
        }).fail(function (err) {
            onError(err);
        });
    }
}

function getHomeAprInvestments(d){
    checkDigestTimeout();
    investments=[];
    $.each(d,function(i,j){   
        investments.push({
            refcode: j.Title,
            title: j.Title0,
            id: j.Id,
            im1: getIM(j.IM_x0028_1_x0029_Id),
            im2: getIM(j.IM_x0028_2_x0029_Id),
            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
            pillar: j.Pillar,
            state: j.Federal_x0020_State,
            district: j.Districts,
            proposal: j.Source_x0020_of_x0020_Proposal,
            output2: j.Output2,
            output3: j.Output3,
            output: j.Output,
            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
            inv_type: j.Investee_x0020_Type,
            proc_type: j.Proc_x002e__x0020_Type,
            proc_cycle: j.Proc_x002e__x0020_Cycle,
            prob_stat: j.Problem_x0020_Statement,
            just: j.Justification,
            rec: j.Recommendation,
            approval_status:j.Approval_x0020_Status,
            attachments:getAttachmentLinks(j.AttachmentFiles),
            comments:j.Comments,
            socomments:j.SO_x0020_Comments,
            tlcomments:j.TL_x0020_Comments,
        });
    });
    loadInvestments();
}


function loadInvestments(){
    var inv_content = '';
    for(i=0;i<investments.length;i++){
        var inv = investments[i];
        if(i%3 === 0){
            inv_content += '<div class="row">';
        }
        inv_content += '<div class="col-md-4">';
        inv_content += '<div class="box-height" onclick="loadInvestment(\''+inv.refcode+'\')">';
        inv_content += '<div class="col-md-4 h-full div-counter">';
        inv_content += '<p class="p-count">'+pad((i+1)+'',2)+'</p>';
        inv_content += '</div>';
        inv_content += '<div class="col-md-8 h-full div-inv">';
        inv_content += '<p class="p-ref mb5">'+inv.refcode+'</p>';
        inv_content += '<p class="p-title mb5">'+inv.title+'</p>';
        inv_content += '<p class="p-im mb5"><b>IM(1):</b>   '+inv.im1+'</p><p class="m0 p-im"><b>IM(2):</b>   '+inv.im2+'</p></p>';
        inv_content += '</div>';
        inv_content += '</div>';
        inv_content += '</div>';
        if((i - 2) % 3 === 0){
            inv_content += '</div>';
        }
        else if(investments.length - 1 === i){
            inv_content += '</div>';
        }
    }
    $('.loaderwrapper').fadeOut();
    $('#inv-contain').empty().append(inv_content);
    $('#attachFilesContainer input:file').MultiFile({max: 4});
}


function filterInvestments(query){
    var result = investments.filter((el) =>
        el.im1.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
        el.im2.toLowerCase().indexOf(query.toLowerCase()) > -1 ||
        el.title.toLowerCase().indexOf(query.toLowerCase()) > -1);
    loadfiltered(result);
}

function loadfiltered(results){
    var inv_content = '';
    for(i=0;i<results.length;i++){
        var inv = results[i];
        if(i%3 === 0){
            inv_content += '<div class="row">';
        }
        inv_content += '<div class="col-md-4">';
        inv_content += '<div class="box-height" onclick="loadInvestment(\''+inv.refcode+'\')">';
        inv_content += '<div class="col-md-4 h-full div-counter">';
        inv_content += '<p class="p-count">'+pad((i+1)+'',2)+'</p>';
        inv_content += '</div>';
        inv_content += '<div class="col-md-8 h-full div-inv">';
        inv_content += '<p class="p-ref mb5">'+inv.refcode+'</p>';
        inv_content += '<p class="p-title mb5">'+inv.title+'</p>';
        inv_content += '<p class="p-im mb5"><b>IM(1):</b>   '+inv.im1+'</p><p class="m0 p-im"><b>IM(2):</b>   '+inv.im2+'</p></p>';
        inv_content += '</div>';
        inv_content += '</div>';
        inv_content += '</div>';
        if((i - 2) % 3 === 0){
            inv_content += '</div>';
        }
        else if(results.length - 1 === i){
            inv_content += '</div>';
        }
    }
    $('.loaderwrapper').fadeOut();
    $('#inv-contain').empty().append(inv_content);

}

function getMemoForm(){
    $('select').chosen();
    $('#primaryIM').empty().append('<option value="'+_spPageContextInfo.userId+'">'+_spPageContextInfo.userDisplayName+'</option>');

    $('#secondaryIM').empty();
    $(ims).each(function(index,value){
        $('#secondaryIM').append('<option value="'+value.id+'">'+value.name+'</option>');
    });

    $('#primaryIM,#secondaryIM').trigger('chosen:updated');

    $('select#state').change(function() {
        var state = $(this);
        var selected_districts = $.grep(districts, function(element, index) {
            if ($(state).find('option:selected').attr('data-initial') === 'MR') {
                return true;
            } else {
                return element.state === $(state).find('option:selected').attr('data-initial');
            }
        });
        $('select#district').empty();
        var content = '';
        for (var i = 0; i < selected_districts.length; i++) {
            content += '<option value="' + selected_districts[i].name + '">' + selected_districts[i].name + '</option>';
        }
        $('#district').append(content).trigger('chosen:updated');
    });
    $('#memo-form .modal-title').html('New Business Case Memo'); 
    $('#memo-form .save-memo').show();
    $('#memo-form .editatt').hide();

    $('#nextbcmemo').click(function() {
        $('#bcmemo').hide();
        $('#bcmemonext').removeClass('hidden').fadeIn();
    });
    $('#bcprevmemo').click(function() {
        $('#bcmemonext').hide();
        $('#bcmemo').removeClass('hidden').fadeIn();
    });
    $('.save-memo-draft').click(function() {
        saveMemoDraft();
    });
    $('.save-memo').click(function() {
        saveMemo();
    });
    $('#memo-form').on('hidden.bs.modal', function () {
      $("#memo-form input,#memo-form textarea").val("");
      $("#memo-form select").val("").trigger('chosen:updated');
      $("#refno").html("");
      $('#memo-form input:file').MultiFile('reset');
    });

    $('#state').change(function() {
        getReference();
    });
    $('#pillar').change(function() {
        getReference();
    });

    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    $('#fundceiling').bind('textchange', function(event, previousText) {
        var value = removeCommas($(this).val());
        if ($.isNumeric(value)) {
            $(this).val(addCommas(value));
        } else {
            $(this).val('');
        }
    });
    $('input.upload').on('change', function() {
        var path = $(this).val();
        var filename = path.substr(path.lastIndexOf('\\') + 1);
        $(this).closest('.input-group').find('.inputFiles').val(filename);
    });

    $('#memo-form').modal('show');
}


function saveMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to submit business case memo for review and approval?',
        type: 'warning',
        showCancelButton: true,
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Final');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitMemo();
    });
}

function saveMemoDraft() {
    swal({
        title: 'Are you sure?',
        text: '<p>Only the primary and secondary im can view</p>',
        html: true,
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'Save as Draft',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Draft');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitMemo();
    });
}

function submitMemo(){
    var data=[],fileArray=[];
    $('#attachFilesContainer input:file').each(function () {
        if ($(this)[0].files[0]) {                    
            fileArray.push({ 'Attachment': $(this)[0].files[0] });                    
        }
    });

    var dist = $('#district').val();
    if(dist!=null) dist=dist.join(', '),
    data.push({
        'Title': $('#refno-h').val() || '',
        'Pillar': $('#pillar').val(),
        'Title0':$('#bCasetitle').val(),
        'Federal_x0020_State': $('#state').val(),
        'Districts':dist,            
        'Source_x0020_of_x0020_Proposal':$('#proposalSrc').val(),
        'Output2': $('#output2').val(),
        'Output3': $('#output3').val(),
        'Output': $('#output').val(),
        'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
        'Investee_x0020_Type': $('#invtype').val(),
        'Proc_x002e__x0020_Cycle': $('#procurement_cycle').val(),
        'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
        'Problem_x0020_Statement':$('#probstat').val(),
        'Justification':$('#justification').val(),
        'Recommendation':$('#recomhead').val(),
        'IM_x0028_1_x0029_': parseInt($('#primaryIM').val()),
        'IM_x0028_2_x0029_': parseInt($('#secondaryIM').val()),
        'Status': $('#submit-type').val(),
        'Files': fileArray  
    });

    createBMemoWithAttachments('Business Case Memo', data).then(
        function(){
           $('#memo-form').modal('hide');
           swal('Success','Business Memo created successfully','success');
           checkDigestTimeout();
       },
       function(sender, args){
        swal('Error','Error occured' + args.get_message());
    });
    

}
var createBMemoWithAttachments = function(listName, listValues){         
    var fileCountCheck = 0;
    var fileNames;          
    var context = new SP.ClientContext.get_current();
    var dfd = $.Deferred();
    var targetList = context.get_web().get_lists().getByTitle(listName);
    context.load(targetList);
    var itemCreateInfo = new SP.ListItemCreationInformation();
    var listItem = targetList.addItem(itemCreateInfo);
    listItem.set_item('Title', listValues[0].Title);   
    listItem.set_item('Pillar', listValues[0].Pillar);
    listItem.set_item('Title0', listValues[0].Title0); 
    listItem.set_item('Federal_x0020_State', listValues[0].Federal_x0020_State); 
    listItem.set_item('Districts', listValues[0].Districts);
    listItem.set_item('Source_x0020_of_x0020_Proposal', listValues[0].Source_x0020_of_x0020_Proposal);
    listItem.set_item('Output2', listValues[0].Output2);   
    listItem.set_item('Output3', listValues[0].Output3);
    listItem.set_item('Output', listValues[0].Output); 
    listItem.set_item('Est_x002e__x0020_Fund_x0020_ceil', listValues[0].Est_x002e__x0020_Fund_x0020_ceil); 
    listItem.set_item('Investee_x0020_Type', listValues[0].Investee_x0020_Type);
    listItem.set_item('Proc_x002e__x0020_Cycle', listValues[0].Proc_x002e__x0020_Cycle);  
    listItem.set_item('Proc_x002e__x0020_Type', listValues[0].Proc_x002e__x0020_Type);   
    listItem.set_item('Problem_x0020_Statement', listValues[0].Problem_x0020_Statement);
    listItem.set_item('Justification', listValues[0].Justification); 
    listItem.set_item('Recommendation', listValues[0].Recommendation);   
    listItem.set_item('IM_x0028_1_x0029_', listValues[0].IM_x0028_1_x0029_);
    listItem.set_item('IM_x0028_2_x0029_', listValues[0].IM_x0028_2_x0029_); 
    listItem.set_item('Status', listValues[0].Status); 
    listItem.update();
    context.executeQueryAsync(
        function () { console.log('execute update');
        var id = listItem.get_id();
        if (listValues[0].Files.length != 0) {
            if (fileCountCheck <= listValues[0].Files.length - 1) {
                loopFileUpload(listName, id, listValues, fileCountCheck).then(
                    function () {},
                    function (sender, args) {
                        console.log('Error uploading');
                        dfd.reject(sender, args);
                    });
            }
        }
        else {
            dfd.resolve(fileCountCheck);
        }
    },   
    function(sender, args){
        swal('Error','Error occured' + args.get_message(),'error');             
    });
    return dfd.promise();           
}

// function bMemoNoAttach(){
//    var item = {
//     '__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
//     'Title': $('#refno-h').val(),
//     'Pillar': $('#pillar').val(),
//     'Title0': $('#bCasetitle').val(),
//     'Federal_x0020_State': $('#state').val(),
//     'Districts': $('#district').val().join(', '),
//     'Source_x0020_of_x0020_Proposal': $('#proposalSrc').val(),
//     'Output2': $('#output2').val(),
//     'Output3': $('#output3').val(),
//     'Output': $('#output').val(),
//     'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
//     'Investee_x0020_Type': $('#invtype').val(),
//     'Proc_x002e__x0020_Cycle': $('#procurement_cycle').val(),
//     'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
//     'Problem_x0020_Statement': $('#probstat').val(),
//     'Justification': $('#justification').val(),
//     'Recommendation': $('#recomhead').val(),
//     'IM_x0028_1_x0029_Id': parseInt($('#primaryIM').val()),
//     'IM_x0028_2_x0029_Id': parseInt($('#secondaryIM').val()),
//     'Status': $('#submit-type').val()
// };
// postJson(psitelst + '(\'Business Case Memo\')/items', item, success);
// function success(d){
//     $('#memo-form').modal('hide');
//     swal('Success','Business Memo created successfully','success');
//     checkDigestTimeout();
// }
// }

    
function loopFileUpload(listName, id, listValues, fileCountCheck) {
        var dfd = $.Deferred();
        uploadFile(listName, id, listValues[0].Files[fileCountCheck].Attachment).then(
            function (data) {                      
                var objcontext = new SP.ClientContext();
                var targetList = objcontext.get_web().get_lists().getByTitle(listName);
                var listItem = targetList.getItemById(id);
                objcontext.load(listItem);
                objcontext.executeQueryAsync(function () {                                      
                    fileCountCheck++;
                    if (fileCountCheck <= listValues[0].Files.length - 1) {
                        loopFileUpload(listName, id, listValues, fileCountCheck);
                    } else {
                        swal(fileCountCheck +' file(s) uploaded successfully.Reloading page...');
                        setTimeout(function(){location.reload();},4000);
                    }
                },
                function (sender, args) {
                    swal('Error','Error occured' + args.get_message(),'error'); 
                });                  
                
            },
            function (sender, args) {
                console.log('Not uploaded');
                dfd.reject(sender, args);
            });
        return dfd.promise();
    }    


function uploadFile(listName, id, file) {
    var deferred = $.Deferred();
    var fileName = file.name;
    getFileBuffer(file).then(
        function (buffer) {
            var bytes = new Uint8Array(buffer);
            var binary = '';
            for (var b = 0; b < bytes.length; b++) {
                binary += String.fromCharCode(bytes[b]);
            }
            var scriptbase = _spPageContextInfo.webServerRelativeUrl + '/_layouts/15/';
            console.log(' File size:' + bytes.length);
            $.getScript(scriptbase + 'SP.RequestExecutor.js', function () {
                var createitem = new SP.RequestExecutor(_spPageContextInfo.webServerRelativeUrl);
                createitem.executeAsync({
                    url: _spPageContextInfo.webServerRelativeUrl + '/_api/web/lists/GetByTitle(\'' + listName + '\')/items(' + id + ')/AttachmentFiles/add(FileName=\'' + file.name + '\')',
                    method: 'POST',
                    binaryStringRequestBody: true,
                    body: binary,
                    success: fsucc,
                    error: ferr,
                    state: 'Update'
                });
                function fsucc(data) {
                    console.log(data + ' uploaded successfully');
                    deferred.resolve(data);
                }
                function ferr(data) {
                    console.log(fileName + 'not uploaded error');
                    deferred.reject(data);
                }
            });

        },
        function (err) {
            deferred.reject(err);
        }
        );
    return deferred.promise();
}

function getFileBuffer(file) {
    var deferred = $.Deferred();
    var reader = new FileReader();
    reader.onload = function (e) {
        deferred.resolve(e.target.result);
    }
    reader.onerror = function (e) {
        deferred.reject(e.target.error);
    }
    reader.readAsArrayBuffer(file);
    return deferred.promise();
}



function updateSaveMemoDraft() {
    swal({
        title: 'Are you sure?',
        text: '<p>Only the primary and secondary im can view</p>',
        html: true,
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'Save as Draft',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#submit-type').val('Draft');
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        updateMemo();
    });
}


function loadInvestment(refcode){
    $('.loaderwrapper').fadeIn();
    if(getCache('inv_html') === null || !cache_active){
        $('div.content-area.container').empty().load('/sites/ssf2/SiteAssets/SSF/investment.html',function(response, status, xhr) {
            cache('inv_html', response);
            processInvestment(refcode);
        });
    }
    else{
        $('div.content-area.container').empty().append(getCache('inv_html'));
        processInvestment(refcode);
    }
}

function updateMemo(){
    var item = {
        '__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
        'Title': $('#refno-h').val(),
        'Pillar': $('#pillar').val(),
        'Title0':$('#bCasetitle').val(),
        'Federal_x0020_State': $('#state').val(),
        'Districts':$('#district').val().join(', '),            
        'Source_x0020_of_x0020_Proposal':$('#proposalSrc').val(),
        'Output2': $('#output2').val(),
        'Output3': $('#output3').val(),
        'Output': $('#output').val(),
        'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
        'Investee_x0020_Type': $('#invtype').val(),
        'Proc_x002e__x0020_Cycle': $('#procurement_cycle').val(),
        'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
        'Problem_x0020_Statement':$('#probstat').val(),
        'Justification':$('#justification').val(),
        'Recommendation':$('#recomhead').val(),
        'IM_x0028_1_x0029_Id': parseInt($('#primaryIM').val()),
        'IM_x0028_2_x0029_Id': parseInt($('#secondaryIM').val()),
        'Status': $('#submit-type').val(),
    };
    var fileArray=[],listValues=[],listName= 'Business Case Memo',id =$('#bmemoid').val(), fileCountCheck = 0;        

    $('#attachFilesContainer input:file').each(function () {
        if ($(this)[0].files[0]) {                    
            fileArray.push({ 'Attachment': $(this)[0].files[0] });                    
        }
    });

    updateJson(psitelst + '(\''+listName+'\')/items('+id+')', item, success);
    function success(d){
        delete item.__metadata;

        item.Files = fileArray;

        listValues.push(item);

        if (listValues[0].Files.length != 0) {
            if (fileCountCheck <= listValues[0].Files.length - 1) {
                loopFileUpload(listName, id, listValues, fileCountCheck).then(
                    function () {console.log("success upload at 510")},
                    function (sender, args) {
                        console.log('Error uploading');
                        dfd.reject(sender, args);
                    });
            }
        }
        else {
            dfd.resolve(fileCountCheck);
        }
        // $('#memo-form').modal('hide');
        // swal('Success','Business Memo updated successfully','success');
        // checkDigestTimeout();
    }

}

function loadDraftMemos(){
    var s='';
    $.each(draftmemos, function (i, j) {        
     s += '<tr><td>'+j.title+'</td><td>'+j.district+'</td><td>'+j.state+'</td><td>'+j.im1+'</td><td>'+j.im2+'</td><td><a href="#" data-id="'+j.id+'" class="btn btn-orange">Review</a></td></tr>';
 });
    var tbdraftmemo=null;
    $('#tbdraftmemo>tbody').html(s);    
    tbdraftmemo= $('#tbdraftmemo').DataTable({responsive: true});
    $('body').on('click','#tbdraftmemo a',function(){ 
        loadDraftMemo($(this).data('id')); 
    });
}

function getDraftMemo(id){
    var i =  $.grep(draftmemos, function (e, index) {
        return e.id === id;
    });
    if(i.length === 1) return i;    
    else return null;    
}

function loadDraftMemo(id){
    var d =  getDraftMemo(id);
    console.log(d);
    if(d==null) return;
    d=d[0];
    getMemoForm();
    $('.update-memo').click(function() {
        updateMemo();
    });
    $('.save-memo-draft').click(function() {
        updateSaveMemoDraft();
    });
    
    $('#refno').text(d.refcode);
    $('#refno-h').val(d.refcode);
    $('#bmemoid').val(id);
    $('#bCasetitle').val(d.title);
    $('#state').val(d.state.split(','));
    $('#district').val(d.district.split(','));
    $('#output').val(d.output);
    $('#output2').val(d.output2);
    $('#output3').val(d.output3);
    $('#pillar').val(d.pillar);
    $('#fundceiling').val(d.est_fund);
    $('#invtype').val(d.inv_type);
    $('#primaryIM').val(d.im1_id);
    $('#secondaryIM').val(d.im2_id);
    $('#procurement_cycle').val(d.proc_cycle);
    $('input[name=procurement_type][value=\'' + d.proc_type + '\']').prop('checked', true);
    $('#hiddentext').html(d.prob_stat);
    var prob_stat=$('#hiddentext').text();
    $('#hiddentext').html('');
    $('#probstat').html(prob_stat);
    $('#hiddentext').html(d.just);
    var just=$('#hiddentext').text();
    $('#hiddentext').html('');
    $('#justification').html(just);
    $('#hiddentext').html(d.rec);      
    var rec =$('#hiddentext').text();
    $('#rec').html(rec);
    $('#hiddentext').html('');
    createAttachmentLink(d);    
    $("body").on('click','.delatt',function(){
        deleteAttachment( $('#bmemoid').val(),$(this).data('file'),$(this));
    });

    $('#memo-form select').trigger('chosen:updated');
    $('#memo-form .modal-title').html('Edit Business Case Memo'); 
    $('#memo-form .save-memo').hide();
    $('#memo-form .editatt').show();
}

function deleteAttachment(id,filename,elem){
    swal({
        title: 'Delete Attachment',
        text: 'Are you sure you want to delete this attachment?',
        type:'warning',
        showCancelButton: true,},
        function(isConfirm){ 
       if (isConfirm){
          elem.parent().remove();
          var Url = _spPageContextInfo.webAbsoluteUrl + '/_api/web/lists/GetByTitle(\'Business Case Memo\')/GetItemById(' + id + ')/AttachmentFiles/getByFileName(\'' + filename+ '\')'; 
          $.ajax({ 
            url: Url, 
            type: 'DELETE', 
            contentType: 'application/json;odata=verbose', 
            headers: { 
                'X-RequestDigest': $('#__REQUESTDIGEST').val(), 
                'X-HTTP-Method': 'DELETE', 
                'Accept': 'application/json;odata=verbose' 
            }, 
            success: function (data) {                 
                removeAttachment(filename,id);
                var d =  getDraftMemo(id);                
                createAttachmentLink(d[0]);
                swal('success','Attachment removed successfully.','success');
                //setTimeout(function(){location.reload();},4000);
            }, 
            error: onError
        }); 
          
      }
  });
}

function createAttachmentLink(d){
    $('.editatt div').html(d.attachments);
    if(d.attachments.length>0){
        $('.editatt div a').html();
        $('.editatt div a').each(function(i, obj) {
            var p = $(obj).html();
            p = p.split('</span>');
            p= p[1].trim();
            $(this).after('<i class="fa fa-trash delatt text-danger" style="cursor:pointer" data-file="'+p+'"></i>');
        });
    }
}

function removeAttachment(filename,id){
    var dmemo = getDraftMemo(id);
    if(dmemo==null) return;
    var d=dmemo[0];
    var links=d.attachments.split(",");
    for (var i = links.length - 1; i >= 0; i--) {
        var ind =links[i].indexOf(filename);
        if(ind!== -1 || links[i].trim() ==""){
            links.splice(ind, 1);
        }
    }
    var datt ="",newdmemo =dmemo[0];
    if(links.length>0) {
        datt = links.join(", ");
        newdmemo.attachments = datt;
    }

    var index = draftmemos.indexOf(dmemo);

    if (index !== -1) {
        draftmemos[index] = newdmemo;
    }

}