function getMemoList(){    
    allinvestments = [];
    $('.loaderwrapper').fadeIn();
    RestCalls('lists/GetByTitle(\'Business Case Memo\')/items?$select=*,AttachmentFiles,IM_x0028_1_x0029_/Title,IM_x0028_1_x0029_/Id,IM_x0028_2_x0029_/Title,IM_x0028_2_x0029_/Id&$expand=AttachmentFiles,IM_x0028_1_x0029_,IM_x0028_2_x0029_',
    	function(d){
            $.each(d.results,function(i,j){            
		        allinvestments.push({
		            refcode: j.Title,
		            title: j.Title0,
		            id: j.Id,
		            im1: getIM(j.IM_x0028_1_x0029_Id),
		            im2: getIM(j.IM_x0028_2_x0029_Id),
		            im1_id: parseInt(j.IM_x0028_1_x0029_Id),
		            im2_id: parseInt(j.IM_x0028_2_x0029_Id),
		            pillar: j.Pillar,
		            state: j.Federal_x0020_State,
		            district: j.Districts,
		            proposal: j.Source_x0020_of_x0020_Proposal,
		            output2: j.Output2,
		            output3: j.Output3,
		            output: j.Output,
		            est_fund: j.Est_x002e__x0020_Fund_x0020_ceil,
		            inv_type: j.Investee_x0020_Type,
		            proc_type: j.Proc_x002e__x0020_Type,
		            proc_cycle: j.Proc_x002e__x0020_Cycle,
		            prob_stat: j.Problem_x0020_Statement,
		            just: j.Justification,
		            rec: j.Recommendation,
		            approval_status:j.Approval_x0020_Status,
		            attachments:getAttachmentLinks(j.AttachmentFiles),
		            comments:j.Comments
		        });		                 
		    });
             populateMemo();  
        });    
}

function editMemo(){
    $('#tab-2 select').prop('disabled',false).trigger('chosen:updated');
    $('.bbs_case_table .read').hide();
    $('.bbs_case_table .edit').show();
    $('#tab-2 input,#tab-2 textarea ').prop('disabled',false);
} 

function populateMemo(){
    $('.loaderwrapper').fadeOut();    
    var s='',cancelct=0,complct=0,pendingct=0;
    $.each(allinvestments, function (i, j) {        
         s += '<tr><td>'+j.title+'</td><td>'+j.district+'</td><td>'+j.state+'</td><td>'+j.im1+'</td><td>'+j.im2+'</td><td>'+j.approval_status+'</td><td><a href="#" data-id="'+j.id+'" class="btn';

        if(j.approval_status=='Completed'){  
            s +=' btn-default">View</a></td></tr>';             
            complct++;
        }else if(j.approval_status=='Rejected'){
            s +=' btn-default">View</a></td></tr>';
            cancelct++;
        }else if(j.approval_status=='Pending'){
            s +=' btn-orange">Review</a></td></tr>';
            pendingct++;
        }else{
            s +=' btn-default">View</a></td></tr>';
        }             
    });
    $('#pending .btn-orange span').text('('+pendingct+')');
    $('#compvared .btn-orange span').text('('+complct+')');
    $('#approved .btn-orange span').text('('+investments.length+')');
    $('#cancelled .btn-orange span').text('('+cancelct+')');
    $('#tbcase>tbody').html(s);
    tbcase=null;
    tbcase= $('#tbcase').DataTable({responsive: true});
    $('.btn-group').click(function(){
        var val = $(this).attr('id');
        val = val.charAt(0).toUpperCase() + val.slice(1);
        tbcase.column(5) .search( val ? '^'+val+'$' : '', true, false ).draw();
    });

    $('body').on('click','#tbcase a',function(){
        var id=$(this).data('id');
        trform= getInvestment(id)[0]; 
        $('.bbs-ref').text(trform.refcode);
        $('#bbs-title').val(trform.title);
        $('#bbs-title1').text(trform.title);
        $('#state').val(trform.state);
        $('#district').val(trform.district);
        $('#primaryIM').val(trform.im1_id);
        $('#secondaryIM').val(trform.im2_id);
        $('#proposalSrc').val(trform.proposal);
        $('#output3').val(trform.output3);
        $('#output2').val(trform.output2);
        $('#output').val(trform.output);
        $('#fundceiling').val(addCommas(trform.est_fund));
        $('#inv_type').val(trform.inv_type);
        $('input[name=procurement_type][value=\'' + trform.proc_type + '\']').prop('checked', true);
        $('#proc_cycle').val(trform.proc_cycle);
        $('#hiddentext').html(trform.prob_stat);
        var prob_stat =$('#hiddentext').text();
        $('#hiddentext').html('');
        $('#prob_stat').html(prob_stat);
        $('#hiddentext').html(trform.just);
        var just=$('#hiddentext').text();
        $('#hiddentext').html('');
        $('#just').html(just);
        $('#hiddentext').html(trform.rec);      
        var rec =$('#hiddentext').text();
        $('#rec').html(rec);
        $('#hiddentext').html('');    
        $('#hiddentext').html(trform.comments);
        var c = $('#hiddentext').text();
        $('#comment').html(c);
        $('#hiddentext').html('');    
        $('#attachments').html(trform.attachments);
        $('.bbs_case_table select').chosen();
        $('[data-tab=\'tab-2\']').removeClass('hidden').trigger('click'); 
        $('.bbs_case_table textarea').each(function() {
            $(this).height($(this).prop('scrollHeight'));
        });   
        if(trform.status=='Approved'){
            $('.bbs_case_table button.btn').hide();
        }          
    });
}

function getInvestment(id){
    var i =  $.grep(allinvestments, function (e, index) {
        return e.id === id;
    });
    if(i.length === 1) return i;    
    else return null;    
}

function submitApproveMemo(){
    var item = {'__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },'Approval_x0020_Status':'Approved','Comments':$('#comment').html()};
    updateJson(psitelst+'(\'Business Case Memo\')/items('+trform.id+')', item, success);
    function success(data) {                        
        swal('success','Business Case Memo approved successfully','success');
        var i = getInvId;
        allinvestments[i].comment= $('#comment').val();
        allinvestments[i].approval_status ='Approved';
        $('[data-tab=\'tab-1\']').trigger('click');
        $('[data-tab=\'tab-2\']').addClass('hidden');
    } 
}


function submitRejectMemo(){
    var item = {'__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },'Approval_x0020_Status':'Rejected','Comments':$('#comment').val()};  
    updateJson(psitelst+'(\'Business Case Memo\')/items('+trform.id+')', item, success);
    function success(data) {                        
        swal('success','Business Case Memo rejected successfully','success');  
        var i = getInvId;
        allinvestments[i].comment= $('#comment').val();
        allinvestments[i].approval_status ='Rejected';
        $('[data-tab=\'tab-1\']').trigger('click');
        $('[data-tab=\'tab-2\']').addClass('hidden');
    } 
}


function saveBMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to submit changes to the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitBMemo();
    });
}

function submitBMemo(){
 var item = {
            '__metadata': { 'type': 'SP.Data.Business_x0020_Case_x0020_MemoListItem' },
            'Title': $('.bbs-ref').text(),
            'Federal_x0020_State': $('#state').val(),
            'Districts':$('#district').val().join(', '),            
            'Source_x0020_of_x0020_Proposal':$('#proposalSrc').val(),
            //"Objective": app.trform.objective,
            'Output': $('#output').val(),
            'Output2': $('#output2').val(),
            'Output3': $('#output3').val(),
            'Est_x002e__x0020_Fund_x0020_ceil': $('#fundceiling').val(),
            'Investee_x0020_Type':$('#inv_type').val(),
            'Proc_x002e__x0020_Cycle': $('#proc_cycle').val(),
            'Proc_x002e__x0020_Type': $('.procurement_type:checked').val(),
            'Problem_x0020_Statement':$('#prob_stat').html(),
            'Justification':$('#just').val(),
            'Recommendation':$('#rec').html(),
            'IM_x0028_1_x0029_Id': parseInt($('#primaryIM').val()),
            'IM_x0028_2_x0029_Id': parseInt($('#secondaryIM').val()),
            'Comments':$('#comment').val()          
        };
    updateJson(psitelst+'(\'Business Case Memo\')/items('+trform.id+')', item, success);
    function success(data) {                        
        swal('success','Business Case Memo updated successfully','success');  
        var i = getInvId;
        allinvestments[i].refcode= $('.bbs-ref').text();
        allinvestments[i].state= $('#state').val();
        allinvestments[i].district=('#district').val();            
        allinvestments[i].proposal=$('#proposalSrc').val();
            //"Objective": app.trform.objective;
        allinvestments[i].output= $('#output').val();
        allinvestments[i].output2= $('#output2').val();
        allinvestments[i].output3= $('#output3').val();
        allinvestments[i].est_fund= $('#fundceiling').val();
        allinvestments[i].inv_type=$('#inv_type').val();
        allinvestments[i].proc_cycle= $('#proc_cycle').val();
        allinvestments[i].proc_type= $('.procurement_type:checked').val();
        allinvestments[i].prob_stat=$('#prob_stat').html();
        allinvestments[i].just=$('#just').val();
        allinvestments[i].rec=$('#rec').html();
        allinvestments[i].im1_id= parseInt($('#primaryIM').val());
        allinvestments[i].im2_id= parseInt($('#secondaryIM').val());
        allinvestments[i].im1=getIM($('#primaryIM').val());
        allinvestments[i].im2=getIM($('#secondaryIM').val());
        allinvestments[i].comment= $('#comment').val();
        allinvestments[i].approval_status ='Comments';         
        $('[data-tab=\'tab-1\']').trigger('click');
        $('[data-tab=\'tab-2\']').addClass('hidden');
    }     
}

function saveRejectMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to reject the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitRejectMemo();
    });
}


function saveApproveMemo() {
    swal({
        title: 'Are you sure?',
        text: 'You want to Approve the business case memo?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        $('#fundceiling').val(removeCommas($('input#fundceiling').val()));
        submitApproveMemo();
    });
}


getInvId = $.map(allinvestments, function(o, i) {
    if(o.id == trform.id) {
        return i;
    }
})[0];


function processInvestment(refcode){
    $('.loaderwrapper').fadeOut();
    loadTabs();
    var inv =  $.grep(investments, function (element, index) {
        return element.refcode === refcode;
    })[0];

    current_inv = inv;

    $('.bbs-ref').html(inv.refcode);
    $('.bbs-title').html(inv.title);
    $('.bbs-state').html(inv.state);
    $('.bbs-districts').html(inv.district);
    $('.bbs-im1').html(inv.im1);
    $('.bbs-im2').html(inv.im2);
    $('.bbs-pillar').html(inv.pillar);
    $('.bbs-prop').html(inv.proposal);
    $('.bbs-output2').html(inv.output2);
    $('.bbs-output3').html(inv.output3);
    $('.bbs-output').html(inv.output);
    $('.bbs-fund').html(inv.est_fund);
    $('.bbs-inv_type').html(inv.inv_type);
    $('.bbs-proc_type').html(inv.proc_type);
    $('.bbs-proc_cycle').html(inv.proc_cycle);
    $('.bbs-prob').html(inv.prob_stat);
    $('.bbs-just').html(inv.just);
    $('.bbs-rec').html(inv.rec);
    $('.bbs-tlcomment').html(inv.tlcomments);
    $('.bbs-socomment').html(inv.socomments);
      
    //#TODO confirm if needed since James said button should always appear
    // RestCalls('lists/GetByTitle(\'Decision Memo\')/items?$filter=Ref_x002e_Code eq \''+inv.refcode+'\'',function(d){
    //     if(d.results.length === 0){
    //         $('.btn-create-memo').removeClass('hidden');
    //     }
    //     else{
    //         $('.btn-create-memo').addClass('hidden');
    //     }        
    // });   

    fixIframe();
}

