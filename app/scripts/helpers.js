
function getCache(key){
    if(localStorage.getItem(key) !== null) {
        return isJson(localStorage.getItem(key));
    }
    else{
        return null;
    }
}

function isJson(item) {
    item = typeof item !== 'string' ? JSON.stringify(item) : item;

    try {
        var json =  JSON.parse(item); if (typeof json === 'object' && json !== null) {
            return json;
        }
        return item;
    } catch (e) {
        return item;
    }
}

function isInt(value) {
    if (isNaN(value)) {
        return false;
    }
    var x = parseFloat(value);
    return (x | 0) === x;
}

function RestCalls(u,f) {   
   return $.ajax({
        url: siteUrl+u,
        method: 'GET',
        headers: {'Accept': 'application/json; odata=verbose'},
        success: function (data) {f(data.d);},
        error: onError        
    });
}

function onError(e) {
    checkDigestTimeout();
    //var msg=JSON.parse(e.responseText);
    console.log(e.responseText);
    swal('Error',e.responseText,'error');
}

function postJson(Uri, payload, success) {    
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: { 'Accept': 'application/json;odata=verbose', 'X-RequestDigest': $('#__REQUESTDIGEST').val() },
        success: success,
        error: onError
    });
}

function updateJson(Uri, payload, success) {
    $.ajax({
        url: Uri,
        type: 'POST',
        data: JSON.stringify(payload),
        contentType: 'application/json;odata=verbose',
        headers: {
            'Accept': 'application/json;odata=verbose','X-RequestDigest': $('#__REQUESTDIGEST').val(),
            'X-HTTP-Method': 'MERGE','If-Match': '*'
        },
        success: success,
        error: onError
    });
}

function checkDigestTimeout(){
    UpdateFormDigest(_spPageContextInfo.webServerRelativeUrl, _spFormDigestRefreshInterval);
    if(getCache('digest_time')){
        var d_time = moment(getCache('digest_time'));
        if(d_time.add((parseInt(getCache('digest_timeout')) - 300), 'seconds').isSameOrAfter(moment.now())){
            renewDigest();
        }
    }else{
       renewDigest(); 
    }
}

function renewDigest() {
    var item={};
    postJson(_spPageContextInfo.webAbsoluteUrl + '/_api/contextinfo', item, success);
    function success(d){
        $('#__REQUESTDIGEST').val(d.d.GetContextWebInformation.FormDigestValue);
        cache('digest_time', moment().format('DD-MM-YYYY HH:MM:SS'));
        cache('digest_timeout', d.d.GetContextWebInformation.FormDigestTimeoutSeconds);
    }
}

function cache(key,value){
    if(typeof value === 'object' ){
        localStorage.setItem(key, JSON.stringify(value));
    }
    else{
        localStorage.setItem(key, value);
    }
}

var addCommas = function(input){
    // If the regex doesn't match, `replace` returns the string unmodified
    return (input.toString()).replace(
        // Each parentheses group (or 'capture') in this regex becomes an argument
        // to the function; in this case, every argument after 'match'
        /^([-+]?)(0?)(\d+)(.?)(\d+)$/g, function(match, sign, zeros, before, decimal, after) {

            // Less obtrusive than adding 'reverse' method on all strings
            var reverseString = function(string) { return string.split('').reverse().join(''); };

            // Insert commas every three characters from the right
            var insertCommas  = function(string) {

                // Reverse, because it's easier to do things from the left
                var reversed = reverseString(string);

                // Add commas every three characters
                var reversedWithCommas = reversed.match(/.{1,3}/g).join(',');

                // Reverse again (back to normal)
                return reverseString(reversedWithCommas);
            };

            // If there was no decimal, the last capture grabs the final digit, so
            // we have to put it back together with the 'before' substring
            return sign + (decimal ? insertCommas(before) + decimal + after : insertCommas(before + after));
        }
        );
};

var removeCommas = function(input){
    return input.replace(/,/g , '');
};

function goto(url) {
    window.location.href = url;
}
var Tawk_API = Tawk_API || {},
Tawk_LoadStart = new Date();
(function() {
    var s1 = document.createElement('script'),
    s0 = document.getElementsByTagName('script')[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/59de1db4c28eca75e4625715/default';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
})();
Tawk_API.visitor = {
    name: _spPageContextInfo.userDisplayName,
    email: _spPageContextInfo.userEmail,
};

function changeActiveMenu(el){
    $('nav ul li').removeClass('active');
    $(el).addClass('active');
}

function fixIframe(){
    $('.MainIframe').contents().find('body').addClass('ms-fullscreenmode');
    $('.MainIframe').contents().find('#s4-ribbonrow,.od-SuiteNav,.Files-leftNav,.od-TopBar-header.od-Files-header,.pageHeader,.ql-editor,#titlerow .ms-table,.sitePage-uservoice-button,.footer').hide().css('display', 'none');
    $('.MainIframe').contents().find('#titlerow').css('background', 'none !important');
    $('.MainIframe').contents().find('.Files-mainColumn').css('left', '0');
    $('.MainIframe').contents().find('.Files-belowSuiteNav').css('top', '0px');
}


function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function loadTabs(){
    checkDigestTimeout();
    $('ul.tabs li').click(function() {
        fixIframe();
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $('#' + tab_id).addClass('current');
    });
}

function getAttachmentLinks(Attachments){  
    var links ='';
    $.each(Attachments, function(index,value){ 
        if(value.ServerRelativeUrl!=null)
            links+='<a target=\'_blank\' href=\''+value.ServerRelativeUrl+'\'><span class=\'fa fa-paperclip\'></span>'+value.FileName+'</a>, ';
 });
    return links;
}

function getIM(id){
    var im =  $.grep(ims, function (element, index) {
        return element.id === id;
    });
    if(im.length === 1){
        return im[0].name;
    }
    else{
        return '';
    }
}

function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('<link rel="stylesheet" href="https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.3/dt-1.10.16/r-2.2.1/datatables.min.css"></link>');
    mywindow.document.write('</head><body >');     
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}