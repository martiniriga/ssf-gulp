
function saveTravel() {
    swal({
        title: 'Travel Request',
        text: 'You want to submit the travel request?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitTravel();
    });
}

function submitTravel(){

    var item = {
        '__metadata': { 'type': 'SP.Data.TRListItem' },
        'Title': 'Travel Request',
        'Purpose': $('#purpose').val(),
        'Dest_x0020_1': $('#dest1').val(),
        'Date_x0020_1': moment($('#date1').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'Dest_x0020_2': $('#dest2').val(),
        'Date_x0020_2': moment($('#date2').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString(),
        'Destinations': $('#dests').val(),
        'Details_x0020_of_x0020_host': $('#host-details').val(),
        'Flights': $('#flights').val(),
        'Accommodation': $('#accommodation').val(),
        'Internal_x0020_Transport': $('#transport').val(),
        'Exta_x0020_Luggage': $('#luggage').val(),
        'Other_x0020_info': $('#other-info').val(),
        'Focal_x0020_PersonId': $('#focal').val()
    };

    if($('#dest3').val()){
        item.Dest_x0020_3 =  $('#dest3').val();
        item.Date_x0020_3 =  moment($('#date3').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString();
    }
    if($('#dest4').val()){
        item.Dest_x0020_4 =  $('#dest4').val();
        item.Date_x0020_4 =  moment($('#date4').val()+' '+moment().format('hh:mm:ss'),'DD/MM/YYYY hh:mm:ss').toISOString();
    }

    postJson(psitelst + '(\'TR\')/items', item, success);
    function success(d){
        $('select').val('').trigger('chosen:updated');
        $('textarea').val('');
        $('input').val('');
        $('#travel-form').data('formValidation').resetForm();
        swal('Success','Travel Request submitted successfully. Awaiting approval','success');
        checkDigestTimeout();
    }
}


function validateLeaveForm() {
    $('#leave-form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            description: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },
            leaveType: {
                validators: {
                    callback: {
                        message: 'The leave type field is required',
                        callback: function(value, validator, $field) {
                            return $('#leave-type').val() !== '';
                        }
                    }
                }
            },
            supervisor: {
                validators: {
                    callback: {
                        message: 'The supervisor field is required',
                        callback: function(value, validator, $field) {
                            return $('#supervisor').val() !== '';
                        }
                    }
                }
            }
        }
    });
}


function validateTravelForm() {
    $('#travel-form').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            purpose: {
                validators: {
                    notEmpty: {
                        message: 'The purpose is required'
                    }
                }
            },
            dest1: {
                validators: {
                    callback: {
                        message: 'The destination 1 field is required',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            date1: {
                validators: {
                    notEmpty: {
                        message: 'The date 1 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 1 field is not a valid date'
                    }
                }
            },
            dest2: {
                validators: {
                    callback: {
                        message: 'The destination 2 field is required',
                        callback: function(value, validator, $field) {
                            return value !== '';
                        }
                    }
                }
            },
            date2: {
                validators: {
                    notEmpty: {
                        message: 'The date 2 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 2 field is not a valid date'
                    }
                }
            },
            date3: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The date 3 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 3 field is not a valid date'
                    }
                }
            },
            date4: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The date 4 field is required'
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The date 4 field is not a valid date'
                    }
                }
            },
            dests: {
                validators: {
                    notEmpty: {
                        message: 'The destinations are required'
                    }
                }
            },
            hostDetails: {
                validators: {
                    notEmpty: {
                        message: 'The host details are required'
                    }
                }
            },
            flights: {
                validators: {
                    notEmpty: {
                        message: 'The flights required field is required'
                    }
                }
            },
            accommodation: {
                validators: {
                    notEmpty: {
                        message: 'The accommodation required field is required'
                    }
                }
            },
            transport: {
                validators: {
                    notEmpty: {
                        message: 'The internal transport required field is required'
                    }
                }
            },
            luggage: {
                validators: {
                    notEmpty: {
                        message: 'The extra luggage to be carried field is required'
                    }
                }
            },
            focal: {
                validators: {
                    callback: {
                        message: 'The focal person is required',
                        callback: function(value, validator, $field) {
                            return $('#focal').val() !== '';
                        }
                    }
                }
            }
        }
    });
}


function loadCalendar(){
    $('#calendar').fullCalendar({
        header: {
            left: '',
            center: 'title',
            right: 'prev,next'
        },
        events: approved_leave_days,
        height: 500,
        fixedWeekCount:false,
        dayClick: function(date, jsEvent, view) {
            var check = moment(date);
            var today = moment.now();
            if(check > today && !isLeaveDay(date.format()))
            {
                if(leave_days.indexOf(date.format()) === -1){
                    leave_days.push(date.format());
                    $('#calendar').fullCalendar( 'addEventSource',[
                    {
                        'id': date.format(),
                        'title': 'Leave',
                        'allDay': true,
                        'start': date.format(),
                        'className': 'hidden'
                    }
                    ]
                    );
                }
                else{
                    leave_days.splice(leave_days.indexOf(date.format()), 1);
                    $(this).removeClass('leave-day-selected');
                    var index = $(this).index();
                    $(this).closest('.fc-bg').next().find('table thead td:eq('+index+')').css('color','#000');
                    $('#calendar').fullCalendar( 'removeEvents', date.format());
                }
                $('.days-leave').text(pad(leave_days.length+'',2));
            }
        },
        eventAfterRender: function(event, element) {
            var index = element.parent().index(); 
            element.parent().closest('.fc-content-skeleton').prev().find('table tbody tr td:eq('+index+')').addClass('leave-day-selected');
            element.parent().closest('tbody').prev().find('tr td:eq('+index+')').css('color','#fff');
        }
    });
}


function saveLeave() {
    swal({
        title: 'Leave Request',
        text: 'You want to submit the leave request?',
        type: 'warning',
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        confirmButtonText: 'Submit',
        confirmButtonColor: '#5cb85c'
    }, function() {
        submitLeave();
    });
}

function submitLeave(){
    leave_days.sort(function(a, b){
        return moment(a,'YYYY-MM-DD') - moment(b,'YYYY-MM-DD');
    });

    leave_days = $.map(leave_days, function(val,i){
        return [moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY')];
    });

    var item = {
        '__metadata': { 'type': 'SP.Data.Leave_x0020_RequestsListItem' },
        'SupervisorId': parseInt($('#supervisor').val()),
        'Days': leave_days.length,
        'Dates': leave_days.join(', '),
        'Description': $('#description').val()
    };
    postJson(_spPageContextInfo.webAbsoluteUrl + '/_api/web/lists/getbytitle(\'Leave Requests\')/items', item, success);
    function success(d){
        $('select').val('').trigger('chosen:updated');
        $('textarea').val('');
        $('#leave-form').data('formValidation').resetForm();
        reloadCalendar();
        swal('Success','Leave Request submitted successfully. Awaiting approval','success');
        checkDigestTimeout();
    }
}


function reloadCalendar(){
    $('.days-leave').text('00');
    leave_days = [];
    $('#calendar').fullCalendar('destroy');
    loadCalendar();
}

function isLeaveDay(l_day){
    var is_leave_day = $.grep(approved_leave_days, function( a_day ) {
        return a_day.start === l_day;
    });
    return is_leave_day.length > 0;
}